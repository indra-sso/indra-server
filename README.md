# Indra

Indra is a Single Sign-On application written with aiohttp in Python

###### Indra comes from Hindu mythology, meaning King of the Heavens

## Table of Contents

[[_TOC_]]

## Features

- Entirely web-based
- Secure
- Easy to implement
- Customizable
- Sexy
- Fast
- Scalable

## External Authentication Support

- OAuth 2
- OIDC
- SAML (Coming Soon!)

## Support

Have questions? Comments? Concerns? Send us a message at [support@enugent.consulting](mailto:support@enugent.consulting)

---

## Secure

Every password in the database is stored using [argon2](https://github.com/P-H-C/phc-winner-argon2). It recently won the Password Hashing Competition, and is on track to become _the_ password hashing standard.

In addition, all tokens, keys, and requests are encrypted over the wire, at rest, and on client computers.

## Scalable

There are _no_ server-specific files, keys, or codes needed to run this. The only necessary system is a database connection, and you're ready to go. Run this anywhere, load balanced, under different domains, however you like. Indra can do it.

## Customizable

You can edit Indra however you like. It is fully customizable, with the ability to use custom HTML, or even write your own API scheme.

---

## Set Up & Install

There are a couple primary ways of installing Indra. You can either download the source and run it from the command line, or you can use our Docker image.

### Requirements

Indra requires:

- A PostgreSQL database, with a database called `indra` under the supplied user (postgres >= 9.2)
- A Redis instance for storing session data

### Run from source

To run Indra from source, it is recommended to `git clone` the repo, then use gunicorn to serve. It is also recommended that Indra does _not_ directly serve requests, and instead is proxied by another service, such as Nginx

For convenience, there is a `scripts/serve.sh` file, which is used in our Docker images, but can be repurposed to serve in a server _other_ than our Docker image.

#### Requirements

- Python 3.7
- Pipenv
- Gunicorn (optional)

Pipenv is used for dependency management.

Never heard of pipenv? Read up on it [here](https://github.com/pypa/pipenv).

#### Run the thing

```bash
$ git clone https://gitlab.com/indra-sso/indra-server.git
$ cd indra
$ pipenv sync
$ pipenv run gunicorn server:application_factory --worker-class aiohttp.GunicornWebWorker
```

Or, alternatively, if you want to run without gunicorn (HIGHLY NOT RECOMMENDED)

```bash
$ git clone https://gitlab.com/indra-sso/indra-server.git
$ cd indra
$ pipenv sync
$ pipenv run python server.py
```

### Docker

[https://gitlab.com/indra-sso/indra-server/container_registry](https://gitlab.com/indra-sso/indra-server/container_registry)

To run the Docker image, there are two required environment variables, described below.

Sample script to run the Indra docker image:

```bash
$ docker pull registry.gitlab.com/indra-sso/indra-server:latest
$ docker run -d -p 80:80 \
    -e POSTGRES_HOST="postgresql://indra:password@1.2.3.4" \
    -e REDIS_HOST="redis://1.2.3.4" \
   registry.gitlab.com/indra-sso/indra-server:latest
```

### Environment Variables

#### POSTGRES_HOST

This is the url to the PostgreSQL server, including the username & password. Indra will automatically use the database called `indra`.

Sample:

`postgresql://indra:password@1.2.3.4`

#### REDIS_HOST

This is the url to the Redis instance. It is recommended that this is hosted on a separate server from the one running Indra, in the event of a server failure.

Sample

`redis://1.2.3.4`

#### REDIS_SESSION_MAX_AGE

This is the max age of the session's cookie in seconds. If it is not set then the default value is `2592000` which is equal to 30 days.

Sample (1 hour session):

`3600`

---

## OAuth 2

### Scopes

- `openid`
- `uname` (Username only)
- `full_name` (Full name)
- `email` (Email only)
- `profile` (Full profile)
- `groups` (Groups only)

You can mix and match scopes as you please, Indra will automatically figure out what data to provide you

### Authorize Endpoint

`GET /oauth/authorize`

Available Parameters:

- `scope`
- `client_id`
- `redirect_uri`
- `state`
- `response_type`
- `code_challenge_method`
- `code_challenge`

This endpoint follows standard oAuth 2 spec. If you have an oauth library, just point it to the `<yourdomain.tld>/oauth/authorize` url

### Token Endpoint

`POST /api/oauth/token`

Available Parameters:

- `client_id`
- `client_secret`
- `grant_type`
- `code_verifier`
- `code`
- `refresh_token`

This endpoint follows standard oAuth 2 spec. If you have an oauth library, just point it to the `<yourdomain.tld>/api/oauth/token` url

### Userinfo Endpoint

`POST /api/oauth/userinfo`

Required Parameters:

- `bearer_token`
  OR
- `Authorization: Bearer <token>` in HTTP headers

This endpoint follows standard oAuth 2 spec. If you have an oauth library, just point it to the `<yourdomain.tld>/api/oauth/userinfo` url

### Token Introspection Endpoint

`POST /api/oauth/token_info`

Required Parameters:

- `token`

This endpoint follows standard oAuth 2 spec. If you have an oauth library, just point it to the `<yourdomain.tld>/api/oauth/token_info` url

### JWKS Endpoint

`GET /api/oauth/jwks`

## SCIM API

This would be if you wanted SCIM access to Indra, for example, using Indra only for sign-in, and not for signups or user management

### Creating a user

`POST /api/scim/user`

Required Parameters:

- `key` (API Key)
- `email`
- `password`
- `given_name`
- `family_name`
- `name` (Username)

### Updating a user

`PUT /api/scim/user`

Required Parameters:

- `key` (API Key)
- `userid` (User's ID)

Available Parameters:

- `email`
- `given_name`
- `family_name`
- `name` (Username)
- `groups`

### Getting a user

`GET /api/scim/user`

Required parameters:

- `key` (API Key)
- `userid` (User's ID)

### Getting a user by email

`GET /api/scim/user/email`

Required parameters:

- `key` (API Key)
- `email` (User's email)

### Update a user's password

`POST /api/scim/user/password`

Required parameters:

- `key` (API Key)
- `userid` (User's ID)
- `password`

`password` must be in plaintext

## Authentication API

This would be used if you wanted to write your own UI

### Logging In

`POST /signin`

Parameters:

- `email`
- `password`

`password` must be in plaintext

### Signing Up

`POST /signup`

Parameters:

- `email`
- `password`
- `passconf`
- `fname`
- `lname`
- `uname`

`password` & `passconf` must be in plaintext

### Signing Out

`GET /signout`

Available parameters:

- `redirect_url` (Where to redirect user after signing out)

### Forgot password

`POST /forgot`

Required Parameters:

- `email`

### Verifying an Email (API)

`GET /email/verify`

Required Parameters:

- `token`

### Resetting a Password (API)

`POST /email/reset`

Parameters:

- `reset_token`
- `newpass`
- `passconf`

`newpass` & `passconf` must be in plaintext
