from endpoints import api, ui


async def add_routes(app):
    await api.add_api_routes(app)
    await ui.add_ui_routes(app)
