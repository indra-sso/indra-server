from aiohttp import web
from aiohttp_session import get_session
import aiohttp_jinja2
from endpoints.middleware.authm import login_required

routes = web.RouteTableDef()


@routes.get("/signin")
@aiohttp_jinja2.template("authenticate.jinja2")
async def signin(request):
    session = await get_session(request)

    error = request.query.get("error")
    errortext = ""
    if error == "invalid_username_or_pass":
        errortext = "Invalid username or password"
    elif error:
        errortext = "An unknown error occurred"

    email = ""
    if "signin_email" in session:
        email = session["signin_email"]

    return {"errortext": errortext, "signin_email": email}


@routes.get("/signin/tfa/otp")
@aiohttp_jinja2.template("tfa_otp_entry.jinja2")
async def signin_tfa_otp(request):
    return {}


@routes.get("/signin/tfa/enable/otp")
@login_required
@aiohttp_jinja2.template("tfa_otp_enable.jinja2")
async def enable_signin_tfa_otp(request):
    session = await get_session(request)
    data = session.get("tfa_data")
    if not data:
        raise web.HTTPSeeOther("/signin")
    return {
        "tfa_secret": data["tfa_data"]["secret"],
        "tfa_qrcode": data["tfa_data"]["qrcode"],
    }


@routes.get("/signin/tfa/enable/sms")
@login_required
@aiohttp_jinja2.template("tfa_sms_enable.jinja2")
async def enable_signin_tfa_sms(request):
    return {}


@routes.get("/signin/tfa/enable/sms/phone")
@login_required
@aiohttp_jinja2.template("tfa_sms_get_phone_number.jinja2")
async def enable_signin_tfa_sms_confirm(request):
    return {}


@routes.get("/signup")
@aiohttp_jinja2.template("signup.jinja2")
async def signup(request):
    return {}


@routes.get("/forgot")
@aiohttp_jinja2.template("forgot.jinja2")
async def forgot(request):
    return {}


@routes.get("/forgot/confirm")
@aiohttp_jinja2.template("forgot_confirm.jinja2")
async def forgot_confirm(request):
    return {}


@routes.get("/email/reset")
@aiohttp_jinja2.template("email_reset.jinja2")
async def email_reset(request):
    reset_token = request.query.get("token")

    error = request.query.get("error")
    errortext = ""

    if error:
        if error == "passwords_do_not_match":
            errortext = "Passwords do not match"
        elif error == "invalid_reset_token":
            errortext = "Invalid reset token. Please close this window and try again"
        else:
            errortext = (
                "An unknown error occurred. Please close this window and try again"
            )

    return {"reset_token": reset_token, "errortext": errortext, "errormsg": error}
