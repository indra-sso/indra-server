from endpoints.ui import userauth, static


async def add_ui_routes(app):
    app.add_routes(userauth.routes)
    app.add_routes(static.routes)
