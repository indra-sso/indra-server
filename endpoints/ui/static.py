from aiohttp import web
from serverconf import config

routes = web.RouteTableDef()


@routes.get("/favicon.ico")
async def favicon(request):
    favicon_url = await config.get_value("FAVICON_URL")
    raise web.HTTPPermanentRedirect(favicon_url)
