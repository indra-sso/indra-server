from aiohttp import web
from aiohttp_session import get_session


@web.middleware  # pylint: disable=E1101
async def auth_middleware(request, handler):
    """Attaches and runs the auth middleware for a given request, and a following handler

    Parameters:
        request (Request): Attached request object
        handler (Handler): Following handler to execute
    """
    session = await get_session(request)
    request["session"] = session
    request["signedin"] = False

    if session.get("signedin") and session.get("userid"):
        request["signedin"] = True

    return await handler(request)


def login_required(func):
    """Decorator to state that login is required to access a given function

    Parameters:
        func (Function): A function that requires login to access
    """

    async def wrapper(request):
        if not request["signedin"]:
            session = await get_session(request)
            session["redirect_after_signin"] = request.path_qs
            raise web.HTTPTemporaryRedirect("/signin")
        return await func(request)

    return wrapper
