from aiohttp import web
from aiohttp_session import get_session, new_session

from endpoints.middleware.authm import login_required

from api import userauth, tfa

from util import errors

from serverconf import config

from db import audit

from security import hashing

routes = web.RouteTableDef()


@routes.post("/signin")
async def signin(request):
    session = await get_session(request)
    data = await request.post()

    email = data.get("email")
    password = data.get("password")
    redirect_after_signin = session.get("redirect_after_signin")
    if not redirect_after_signin:
        redirect_after_signin = "/"

    if not email or not password:
        session["signin_email"] = email
        await audit.create_audit_log_entry(
            "userauth",
            await hashing.hash_sha256(email),
            "signin",
            info={"status": "error", "reason": "invalid_inputs"},
        )
        raise web.HTTPSeeOther("/signin?error=invalid_username_or_pass")

    info = await userauth.handle_signin(email, password)

    if not info["userid"]:
        if info["error"]:
            await audit.create_audit_log_entry(
                "userauth",
                await hashing.hash_sha256(email),
                "signin",
                info={
                    "userid": info["userid"],
                    "status": "error",
                    "reason": info["error"],
                },
            )
            raise web.HTTPSeeOther("/signin?error={}".format(info["error"]))

        session["signin_email"] = email
        await audit.create_audit_log_entry(
            "userauth",
            await hashing.hash_sha256(email),
            "signin",
            info={
                "userid": info["userid"],
                "status": "error",
                "reason": "invalid_username_or_pass",
            },
        )
        raise web.HTTPSeeOther("/signin?error=invalid_username_or_pass")

    session = await new_session(request)
    session["userid"] = info["userid"]
    session["signedin"] = info["signedin"]

    if info["tfa_needed"]:
        session["redirect_after_signin"] = redirect_after_signin
        session["tfa_needed"] = True
        session["tfa_types_avail"] = info["tfa_types_avail"]
        await audit.create_audit_log_entry(
            "userauth",
            await hashing.hash_sha256(email),
            "signin",
            info={"userid": info["userid"], "status": "tfa_needed"},
        )
        if len(info["tfa_types_avail"]) >= 1:
            raise web.HTTPSeeOther("/signin/tfa/{}".format(info["tfa_types_avail"][0]))

        raise web.HTTPSeeOther("/signin/tfa")

    await audit.create_audit_log_entry(
        "userauth",
        await hashing.hash_sha256(email),
        "signin",
        info={"userid": info["userid"], "status": "successful"},
    )

    raise web.HTTPSeeOther(redirect_after_signin)


@routes.get("/")
async def index(request):
    raise web.HTTPTemporaryRedirect(await config.get_value("EMAIL_HOME_URL"))


@routes.post("/api/auth/signin/tfa/enable/{tfa_type}")
@login_required
async def enable_tfa_type(request):
    tfa_type = request.match_info["tfa_type"]
    session = await get_session(request)
    data = await request.post()

    userid = session.get("userid")

    if not userid:
        await audit.create_audit_log_entry(
            "userauth",
            userid,
            "tfa_enable",
            info={"status": "error", "tfa_type": tfa_type, "error": "invalid_session"},
        )
        await errors.raise_bad_request(
            {"status": "error", "message": "invalid_session"}
        )

    additional_data = {}
    if tfa_type == "sms":
        additional_data["phone_number"] = data.get("phone_number")

    ret = await tfa.enable_tfa(userid, tfa_type, additional_data)

    if not ret:
        await audit.create_audit_log_entry(
            "userauth",
            userid,
            "tfa_enable",
            info={"status": "error", "tfa_type": tfa_type, "error": "invalid_tfa_type"},
        )
        await errors.raise_bad_request(
            {"status": "error", "message": "invalid_tfa_type"}
        )

    session["tfa_data"] = ret

    await audit.create_audit_log_entry(
        "userauth",
        userid,
        "enable_tfa",
        info={"status": "successful", "tfa_type": tfa_type},
    )

    raise web.HTTPSeeOther("/signin/tfa/enable/{}".format(tfa_type))


@routes.post("/signin/tfa/{tfa_type}")
async def tfa_signin(request):
    tfa_type = request.match_info["tfa_type"]
    session = await get_session(request)

    userid = session.get("userid")
    redirect_after_signin = session.get("redirect_after_signin")
    if not redirect_after_signin:
        redirect_after_signin = "/"

    if not userid:
        await audit.create_audit_log_entry(
            "userauth",
            userid,
            "signin_tfa",
            info={"status": "error", "tfa_type": tfa_type, "error": "invalid_session"},
        )
        await errors.raise_bad_request(
            {"status": "error", "message": "invalid_session"}
        )

    if not session["tfa_needed"]:
        await audit.create_audit_log_entry(
            "userauth",
            userid,
            "signin_tfa",
            info={"status": "error", "tfa_type": tfa_type, "error": "tfa_not_needed"},
        )
        await errors.raise_bad_request({"status": "error", "message": "tfa_not_needed"})

    data = await request.post()

    tfa_data = data.get("data")
    if not tfa_data or not tfa_type:
        await audit.create_audit_log_entry(
            "userauth",
            userid,
            "signin_tfa",
            info={"status": "error", "tfa_type": tfa_type, "error": "no_tfa_data"},
        )
        await errors.raise_bad_request({"status": "error", "message": "invalid_inputs"})

    valid, error = await tfa.handle_tfa_signin(session["userid"], tfa_type, tfa_data)

    if not valid:
        await audit.create_audit_log_entry(
            "userauth",
            userid,
            "signin_tfa",
            info={"status": "error", "tfa_type": tfa_type},
        )
        await errors.raise_bad_request({"status": "error", "message": error})

    session["signedin"] = True
    session["tfa_needed"] = False

    await audit.create_audit_log_entry(
        "userauth",
        userid,
        "signin_tfa",
        info={"status": "successful", "tfa_type": tfa_type},
    )

    raise web.HTTPSeeOther(redirect_after_signin)


@routes.post("/api/auth/signin/tfa/enable/{tfa_type}/confirm")
@login_required
async def tfa_confirm(request):
    tfa_type = request.match_info["tfa_type"]
    session = await get_session(request)

    userid = session.get("userid")

    if not userid:
        await audit.create_audit_log_entry(
            "userauth",
            userid,
            "confirm_tfa_enable",
            info={"status": "error", "tfa_type": tfa_type, "error": "invalid_session"},
        )
        await errors.raise_bad_request(
            {"status": "error", "message": "invalid_session"}
        )

    data = await request.post()

    tfa_data = data.get("data")
    if not tfa_data or not tfa_type:
        await audit.create_audit_log_entry(
            "userauth",
            userid,
            "confirm_tfa_enable",
            info={"status": "error", "tfa_type": tfa_type, "error": "no_tfa_data"},
        )
        await errors.raise_bad_request({"status": "error", "message": "invalid_inputs"})

    valid, error = await tfa.verify_enable_tfa(session["userid"], tfa_type, tfa_data)

    if not valid:
        await audit.create_audit_log_entry(
            "userauth",
            userid,
            "confirm_tfa_enable",
            info={"status": "error", "tfa_type": tfa_type, "error": "invalid_tfa_data"},
        )
        await errors.raise_bad_request({"status": "error", "message": error})

    del session["tfa_data"]

    raise web.HTTPSeeOther("/")


@routes.post("/signup")
async def signup(request):
    data = await request.post()

    email = data.get("email")
    password = data.get("password")
    passconf = data.get("passconf")
    fname = data.get("fname")
    lname = data.get("lname")
    uname = data.get("uname")

    if not email or not password or not passconf or not fname or not lname or not uname:
        await audit.create_audit_log_entry(
            "userauth",
            await hashing.hash_sha256(email),
            "signup",
            info={"status": "error", "reason": "invalid_inputs"},
        )
        await errors.raise_bad_request({"status": "error", "message": "invalid_inputs"})

    ret, message = await userauth.handle_signup(
        email, password, passconf, fname, lname, uname
    )

    if not ret:
        await audit.create_audit_log_entry(
            "userauth",
            await hashing.hash_sha256(email),
            "signup",
            info={"status": "error", "reason": message},
        )
        await errors.raise_bad_request({"status": "error", "message": message})

    await audit.create_audit_log_entry(
        "userauth",
        await hashing.hash_sha256(email),
        "signup",
        info={"status": "successful"},
    )

    return web.json_response({"status": "done"})


@routes.get("/signout")
async def signout(request):
    session = await get_session(request)

    redirect_url = request.query.get("redirect_url")

    session_id = session.identity

    session["signedin"] = False
    session.invalidate()

    await userauth.invalidate_session(session_id)

    session = await new_session(request)
    # This is done because if you don't set something on the new session, it doesn't stick...
    session["signedin"] = False

    await audit.create_audit_log_entry("userauth", session_id, "signout")

    if redirect_url:
        raise web.HTTPTemporaryRedirect(redirect_url)

    raise web.HTTPTemporaryRedirect("/signin")


@routes.post("/api/auth/tfa/enable/{tfa_type}")
@login_required
async def enable_tfa(request):
    session = await get_session(request)
    tfa_type = request.match_info["tfa_type"]

    userid = session["userid"]

    d = await tfa.enable_tfa(userid, tfa_type)

    if not d["success"]:
        await errors.raise_bad_request({"status": "error", "message": d["message"]})

    return web.json_response({"status": "done", "tfa_data": d["tfa_data"]})


@routes.post("/api/auth/tfa/verify/otp")
@login_required
async def verify_enable_tfa_otp(request):
    session = await get_session(request)
    data = await request.post()

    userid = session["userid"]

    tfa_code = data.get("tfa_code")
    if not tfa_code:
        await errors.raise_bad_request({"status": "error", "message": "invalid_inputs"})

    success, message = await tfa.verify_enable_tfa_otp(userid, tfa_code)

    if not success:
        await errors.raise_bad_request({"status": "error", "message": message})

    return web.json_response({"status": "done"})


@routes.post("/forgot")
async def forgot_password(request):
    data = await request.post()

    email = data.get("email")

    await userauth.forgot_password(email)

    await audit.create_audit_log_entry(
        "userauth",
        await hashing.hash_sha256(email),
        "forgot_password",
        info={"status": "successful"},
    )

    raise web.HTTPSeeOther("/forgot/confirm")


@routes.post("/email/reset")
async def reset_password(request):
    data = await request.post()

    newpass = data.get("newpass")
    passconf = data.get("passconf")
    reset_token = data.get("reset_token")

    if not newpass or not passconf:
        await audit.create_audit_log_entry(
            "userauth",
            reset_token,
            "reset_password",
            info={"status": "error", "reason": "invalid_password"},
        )
        raise web.HTTPSeeOther(
            "/email/reset?reset_token={}&error=invalid_password".format(reset_token)
        )

    valid, msg = await userauth.reset_password(reset_token, newpass, passconf)

    if not valid:
        await audit.create_audit_log_entry(
            "userauth",
            reset_token,
            "reset_password",
            info={"status": "error", "reason": msg},
        )
        raise web.HTTPSeeOther(
            "/email/reset?reset_token={}&error={}".format(reset_token, msg)
        )

    await audit.create_audit_log_entry(
        "userauth", reset_token, "reset_password", info={"status": "successful"}
    )

    raise web.HTTPSeeOther("/signin")
