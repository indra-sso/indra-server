from aiohttp import web
from api import oauth
from util import errors, urls, headers
from db import audit
from security import hashing


routes = web.RouteTableDef()


async def error_redirect(redirect_url, args):
    if not redirect_url:
        await errors.raise_bad_request(args)

    redirect_url = urls.append_query(redirect_url, args)

    raise web.HTTPSeeOther(redirect_url)


@routes.post("/api/oauth/token")
async def oauth2_token(request):
    data = await request.post()

    client_id = data.get("client_id")
    client_secret = data.get("client_secret")
    code_verifier = data.get("code_verifier")

    # Needs to be pulled out to an external function to get client_id and client_secret
    if not client_secret and not code_verifier:
        if "Authorization" in request.headers:
            auth = request.headers["Authorization"]
            d = await headers.parse_authorization_header(auth)
            if "client_id" not in d or "client_secret" not in d:
                await errors.raise_bad_request({"error": "invalid_request"})

            client_id = d["client_id"]
            client_secret = d["client_secret"]

    access_token, audit_log_type, errormsg = await oauth.handle_token_post(
        data, client_id, client_secret
    )

    if not access_token:
        await audit.create_audit_log_entry(
            "oauth",
            client_id,
            audit_log_type,
            info={"client_id": client_id, "status": "error", "reason": errormsg},
        )
        await errors.raise_bad_request({"error": errormsg})

    await audit.create_audit_log_entry(
        "oauth",
        client_id,
        audit_log_type,
        info={
            "client_id": client_id,
            "status": "successful",
            "access_token_hash": await hashing.hash_sha256(access_token),
        },
    )
    return web.json_response(access_token)


@routes.post("/api/oauth/token_info")
async def oauth2_token_introspec(request):
    data = await request.post()

    token = data.get("token")

    if not token:
        await errors.raise_bad_request({"error": "invalid_request"})

    token_data = await oauth.get_token_info(token)

    await audit.create_audit_log_entry(
        "oauth",
        await hashing.hash_sha256(token),
        "token_info",
        info={"token_data": token_data, "status": "success"},
    )

    return web.json_response(token_data)
