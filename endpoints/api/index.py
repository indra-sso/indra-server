from aiohttp import web
import subprocess

routes = web.RouteTableDef()


@routes.get("/version")
async def get_version(request):
    try:
        commit_hash = (
            subprocess.check_output(["git", "rev-parse", "HEAD"])
            .decode("utf-8")
            .strip()
        )
        return web.json_response({"version": commit_hash})
    except (subprocess.CalledProcessError, FileNotFoundError):
        try:
            print("Unable to get commit hash for version")
            with open("VERSION", "r") as f:
                version_data = f.read().strip()
                return web.json_response({"version": version_data})
        except FileNotFoundError:
            return "unknown"
