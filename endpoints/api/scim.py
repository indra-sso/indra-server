from aiohttp import web
from api import scim, admin
from util import errors
from db import audit
from security import hashing

routes = web.RouteTableDef()


async def check_api_key(api_key, scopes):
    if isinstance(scopes, str):
        scopes = [scopes]
    for s in scopes:
        if await admin.validate_api_key(api_key, s):
            return

    await errors.raise_unauthorized()


@routes.post("/api/scim/user")
async def scim_create_user(request):
    data = await request.post()

    api_key = data.get("key")
    await check_api_key(api_key, "scim_admin")

    email = data.get("email")
    password = data.get("password")
    fname = data.get("given_name")
    lname = data.get("family_name")
    uname = data.get("name")

    if not email or not password or not fname or not lname or not uname:
        await audit.create_audit_log_entry(
            "scim",
            await hashing.hash_sha256(api_key),
            "create_user",
            info={
                "userid": await hashing.hash_sha256(email),
                "status": "error",
                "reason": "invalid_inputs",
            },
        )
        await errors.raise_bad_request({"status": "error", "message": "invalid_inputs"})

    valid, msg = await scim.create_user(email, password, fname, lname, uname)
    if not valid:
        await audit.create_audit_log_entry(
            "scim",
            await hashing.hash_sha256(api_key),
            "create_user",
            info={
                "userid": await hashing.hash_sha256(email),
                "status": "error",
                "reason": msg,
            },
        )
        await errors.raise_bad_request({"status": "error", "message": msg})

    await audit.create_audit_log_entry(
        "scim",
        await hashing.hash_sha256(api_key),
        "create_user",
        info={"email_hash": await hashing.hash_sha256(email), "status": "successful"},
    )

    return web.json_response({"status": "done"})


@routes.put("/api/scim/user")
async def scim_put_user(request):
    data = await request.post()

    api_key = data.get("key")
    await check_api_key(api_key, "scim_admin")

    userid = data.get("userid")
    email = data.get("email")
    fname = data.get("given_name")
    lname = data.get("family_name")
    uname = data.get("name")
    groups = data.get("groups")
    image = data.get("image")

    fields = {}
    if email:
        fields["email"] = email
    if fname:
        fields["fname"] = fname
    if lname:
        fields["lname"] = lname
    if uname:
        fields["uname"] = uname
    if groups:
        fields["groups"] = groups
    if image:
        fields["image"] = groups

    if not userid:
        await audit.create_audit_log_entry(
            "scim",
            await hashing.hash_sha256(api_key),
            "update_user",
            info={
                "userid": userid,
                "status": "error",
                "reason": "invalid_inputs",
                "updated_fields": fields,
            },
        )
        await errors.raise_bad_request({"status": "error", "message": "invalid_inputs"})

    try:
        userid_int = int(userid)
    except:
        await audit.create_audit_log_entry(
            "scim",
            await hashing.hash_sha256(api_key),
            "update_user",
            info={
                "userid": userid,
                "status": "error",
                "reason": "invalid_inputs",
                "updated_fields": fields,
            },
        )
        await errors.raise_bad_request({"status": "error", "message": "invalid_inputs"})

    await scim.update_user(userid, email, fname, lname, uname, groups, image)

    await audit.create_audit_log_entry(
        "scim",
        await hashing.hash_sha256(api_key),
        "update_user",
        info={"userid": userid, "status": "successful", "updated_fields": fields},
    )

    return web.json_response({"status": "done"})


@routes.put("/api/scim/user/fields")
async def scim_put_user_custom_fields(request):
    data = await request.post()

    api_key = data.get("key")
    await check_api_key(api_key, "scim_admin")

    userid = data.get("userid")

    if not userid:
        await audit.create_audit_log_entry(
            "scim",
            await hashing.hash_sha256(api_key),
            "update_user_fields",
            info={"userid": userid, "status": "error", "reason": "invalid_inputs"},
        )
        await errors.raise_bad_request({"status": "error", "message": "invalid_inputs"})

    try:
        userid_int = int(userid)
    except:
        await audit.create_audit_log_entry(
            "scim",
            await hashing.hash_sha256(api_key),
            "update_user_fields",
            info={"userid": userid, "status": "error", "reason": "invalid_inputs"},
        )
        await errors.raise_bad_request({"status": "error", "message": "invalid_inputs"})

    fields = dict(data)
    del fields["userid"]
    del fields["key"]

    await scim.update_user_fields(userid, fields)

    await audit.create_audit_log_entry(
        "scim",
        await hashing.hash_sha256(api_key),
        "update_user_fields",
        info={"userid": userid, "status": "successful", "fields": fields},
    )

    return web.json_response({"status": "done"})


@routes.get("/api/scim/user/fields")
async def scim_get_user_custom_fields(request):
    api_key = request.query.get("key")
    await check_api_key(api_key, ["scim_readonly", "scim_admin"])

    data = request.query

    fields = dict(data)
    del fields["key"]

    limit = request.query.get("limit")
    offset = request.query.get("offset")
    try:
        if limit:
            limit = int(limit)
        if offset:
            offset = int(offset)
    except:
        await audit.create_audit_log_entry(
            "scim",
            await hashing.hash_sha256(api_key),
            "get_user_fields",
            info={"status": "error", "fields": fields, "reason": "invalid_inputs"},
        )
        await errors.raise_bad_request({"status": "error", "message": "invalid_inputs"})

    users = await scim.get_user_by_fields(fields, limit=limit, offset=offset)

    await audit.create_audit_log_entry(
        "scim",
        await hashing.hash_sha256(api_key),
        "get_user_fields",
        info={"status": "successful", "fields": fields},
    )

    return web.json_response(users)


@routes.get("/api/scim/user")
async def scim_get_user(request):
    api_key = request.query.get("key")
    await check_api_key(api_key, ["scim_readonly", "scim_admin"])

    userid = request.query.get("userid")
    limit = request.query.get("limit")
    offset = request.query.get("offset")
    order_by = request.query.get("order_by")
    try:
        if limit:
            limit = int(limit)
        if offset:
            offset = int(offset)
    except:
        await audit.create_audit_log_entry(
            "scim",
            await hashing.hash_sha256(api_key),
            "get_user",
            info={"userid": userid, "status": "error", "reason": "invalid_inputs"},
        )
        await errors.raise_bad_request({"status": "error", "message": "invalid_inputs"})

    try:
        if userid:
            userid = int(userid)
    except:
        await audit.create_audit_log_entry(
            "scim",
            await hashing.hash_sha256(api_key),
            "get_user",
            info={"userid": userid, "status": "error", "reason": "invalid_inputs"},
        )
        await errors.raise_bad_request({"status": "error", "message": "invalid_inputs"})

    u = await scim.get_user(userid, limit, offset, order_by)

    if not u:
        await audit.create_audit_log_entry(
            "scim",
            await hashing.hash_sha256(api_key),
            "get_user",
            info={"userid": userid, "status": "error", "reason": "user_not_found"},
        )
        await errors.raise_bad_request({"status": "error", "message": "user_not_found"})

    await audit.create_audit_log_entry(
        "scim",
        await hashing.hash_sha256(api_key),
        "get_user",
        info={"userid": userid, "status": "successful"},
    )

    return web.json_response(u)


@routes.get("/api/scim/user/email")
async def scim_get_user_by_email(request):
    api_key = request.query.get("key")
    await check_api_key(api_key, ["scim_readonly", "scim_admin"])

    email = request.query.get("email")

    if not email:
        await audit.create_audit_log_entry(
            "scim",
            await hashing.hash_sha256(api_key),
            "get_user_by_email",
            info={
                "userid": await hashing.hash_sha256(email),
                "status": "error",
                "reason": "invalid_inputs",
            },
        )
        await errors.raise_bad_request({"status": "error", "message": "invalid_inputs"})

    u = await scim.get_user_by_email(email)

    if not u:
        await audit.create_audit_log_entry(
            "scim",
            await hashing.hash_sha256(api_key),
            "get_user_by_email",
            info={
                "email_hash": await hashing.hash_sha256(email),
                "status": "error",
                "reason": "user_not_found",
            },
        )
        await errors.raise_bad_request({"status": "error", "message": "user_not_found"})

    await audit.create_audit_log_entry(
        "scim",
        await hashing.hash_sha256(api_key),
        "get_user_by_email",
        info={"email_hash": await hashing.hash_sha256(email), "status": "successful"},
    )

    return web.json_response(u)


@routes.get("/api/scim/user/groups")
async def scim_get_user_by_groups(request):
    api_key = request.query.get("key")
    await check_api_key(api_key, ["scim_readonly", "scim_admin"])

    groups = request.query.get("groups")
    limit = request.query.get("limit")
    offset = request.query.get("offset")
    order_by = request.query.get("order_by")
    try:
        if limit:
            limit = int(limit)
        if offset:
            offset = int(offset)
    except:
        await audit.create_audit_log_entry(
            "scim",
            await hashing.hash_sha256(api_key),
            "get_user_by_groups",
            info={"groups": groups, "status": "error", "reason": "invalid_inputs"},
        )
        await errors.raise_bad_request({"status": "error", "message": "invalid_inputs"})

    if not groups:
        await audit.create_audit_log_entry(
            "scim",
            await hashing.hash_sha256(api_key),
            "get_user_by_groups",
            info={"groups": groups, "status": "error", "reason": "invalid_inputs"},
        )
        await errors.raise_bad_request({"status": "error", "message": "invalid_inputs"})

    u = await scim.get_user_by_groups(groups, limit, offset, order_by)

    await audit.create_audit_log_entry(
        "scim",
        await hashing.hash_sha256(api_key),
        "get_user_by_groups",
        info={"groups": groups, "status": "successful"},
    )

    return web.json_response(u)


@routes.post("/api/scim/user/password")
async def scim_change_user_password(request):
    data = await request.post()

    api_key = data.get("key")
    await check_api_key(api_key, "scim_admin")

    userid = data.get("userid")
    newpass = data.get("password")

    if not userid or not newpass:
        await audit.create_audit_log_entry(
            "scim",
            await hashing.hash_sha256(api_key),
            "update_user_password",
            info={"userid": userid, "status": "error", "reason": "invalid_inputs"},
        )

        await errors.raise_bad_request({"status": "error", "message": "invalid_inputs"})

    try:
        userid_int = int(userid)
    except:
        await audit.create_audit_log_entry(
            "scim",
            await hashing.hash_sha256(api_key),
            "update_user_password",
            info={"userid": userid, "status": "error", "reason": "invalid_inputs"},
        )
        await errors.raise_bad_request({"status": "error", "message": "invalid_inputs"})

    await scim.update_password(userid, newpass)

    await audit.create_audit_log_entry(
        "scim",
        await hashing.hash_sha256(api_key),
        "update_user_password",
        info={"userid": userid, "status": "successful"},
    )

    return web.json_response({"status": "done"})


@routes.get("/api/scim/user/search")
async def scim_get_user_by_search(request):
    api_key = request.query.get("key")
    await check_api_key(api_key, ["scim_readonly", "scim_admin"])

    query = request.query.get("search")
    limit = request.query.get("limit")
    offset = request.query.get("offset")
    order_by = request.query.get("order_by")
    try:
        if limit:
            limit = int(limit)
        if offset:
            offset = int(offset)
    except:
        await audit.create_audit_log_entry(
            "scim",
            await hashing.hash_sha256(api_key),
            "get_user_by_search",
            info={"query": query, "status": "error", "reason": "invalid_inputs"},
        )
        await errors.raise_bad_request({"status": "error", "message": "invalid_inputs"})

    if not query:
        query = ""

    total, rows = await scim.search_user(query, limit, offset, order_by)

    await audit.create_audit_log_entry(
        "scim",
        await hashing.hash_sha256(api_key),
        "get_user_by_search",
        info={"query": query, "status": "successful"},
    )

    return web.json_response({"total": total, "rows": rows})
