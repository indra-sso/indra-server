from aiohttp import web
from api import email
from util import errors
from db import audit

routes = web.RouteTableDef()


@routes.get("/email/verify")
async def email_verify(request):
    token = request.query.get("token")

    if not token:
        await errors.raise_bad_request({"status": "error", "reason": "invalid_token"})

    success = await email.verify_email(token)

    if not success:
        await errors.raise_bad_request({"status": "error", "reason": "invalid_token"})

    await audit.create_audit_log_entry("email", token, "email_verify")

    return web.json_response({"status": "done"})


@routes.get("/email/password_reset")
async def email_password_reset(request):
    token = request.query.get("token")
    password = request.query.get("password")
    passconf = request.query.get("passconf")

    if not token or not password or not passconf:
        await errors.raise_bad_request({"status": "error", "reason": "invalid_input"})

    ret = await email.password_reset(token, password, passconf)

    if ret:
        await errors.raise_bad_request({"status": "error", "reason": ret})

    await audit.create_audit_log_entry("email", token, "password_reset")

    return web.json_response({"status": "done"})
