import asyncio
from aiohttp import web
from serverconf import config


routes = web.RouteTableDef()

ROOT_URL = "https://{}".format(
    asyncio.get_event_loop().run_until_complete(config.get_value("SITE_DOMAIN"))
)

OIDC_CONFIGURATION = {
    "issuer": ROOT_URL,
    "authorization_endpoint": "{}/oauth/authorize".format(ROOT_URL),
    "token_endpoint": "{}/api/oauth/token".format(ROOT_URL),
    "userinfo_endpoint": "{}/api/oauth/userinfo".format(ROOT_URL),
    "jwks_uri": "{}/api/oauth/jwks".format(ROOT_URL),
    "scopes_supported": ["openid", "uname", "full_name", "email", "profile", "groups"],
    "response_types_supported": [
        "code",
    ],
    "grant_types_supported": [
        "authorization_code",
    ],
    "subject_types_supported": ["public"],
    "token_endpoint_auth_signing_alg_values_supported": ["ES384"],
    "request_uri_parameter_supported": True,
}


@routes.get("/.well-known/openid-configuration")
async def oidc_get_configuration(request):
    return web.json_response(OIDC_CONFIGURATION)
