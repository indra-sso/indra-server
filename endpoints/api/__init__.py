from endpoints.api import (
    userauth,
    oauth_userview,
    oauth_token,
    oauth_api,
    scim,
    email,
    well_known,
    admin,
    index,
)


async def add_api_routes(app):
    app.add_routes(userauth.routes)
    app.add_routes(oauth_userview.routes)
    app.add_routes(oauth_token.routes)
    app.add_routes(oauth_api.routes)
    app.add_routes(scim.routes)
    app.add_routes(email.routes)
    app.add_routes(well_known.routes)
    app.add_routes(admin.routes)
    app.add_routes(index.routes)
