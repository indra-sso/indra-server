from aiohttp import web
from aiohttp_session import get_session
from aiohttp_jinja2 import render_template

from endpoints.middleware.authm import login_required

from api import oauth

from util import errors, urls

from db import audit


routes = web.RouteTableDef()


async def error_redirect(redirect_url, args):
    if not redirect_url:
        await errors.raise_bad_request(args)

    redirect_url = urls.append_query(redirect_url, args)

    raise web.HTTPSeeOther(redirect_url)


@routes.get("/oauth/authorize")
@login_required
async def oauth2_authorize_get(request):
    session = await get_session(request)

    scopes = request.query.get("scope")
    client_id = request.query.get("client_id")
    redirect_url = request.query.get("redirect_uri")
    state = request.query.get("state")
    response_type = request.query.get("response_type")
    code_challenge_method = request.query.get("code_challenge_method")
    code_challenge = request.query.get("code_challenge")

    if not response_type:
        args = {"status": "error", "reason": "invalid_response_type", "state": state}
        await error_redirect(redirect_url, args)

    if not client_id:
        client_id = request.query.get("oauth_consumer_key")
        if not client_id:
            args = {"status": "error", "reason": "invalid_client_id", "state": state}
            await error_redirect(redirect_url, args)

    valid_client_id = await oauth.validate_client_id(
        client_id, redirect_url=redirect_url
    )
    if not valid_client_id:
        args = {"status": "error", "reason": "invalid_client_id", "state": state}
        await error_redirect(redirect_url, args)

    client_name = await oauth.get_client_name(client_id)

    if not scopes:
        scopes = await oauth.get_scopes(client_id)

    scope_text = await oauth.get_scope_text(scopes)

    if not redirect_url:
        redirect_url = await oauth.get_redirect_url(client_id)

    session["oauth2_authorize_scopes"] = scopes
    session["oauth2_authorize_client_id"] = client_id
    session["oauth2_authorize_redirect_url"] = redirect_url
    session["oauth2_response_type"] = response_type
    session["code_challenge"] = code_challenge
    session["code_challenge_method"] = code_challenge_method

    if await oauth.must_authorize(client_id):
        return render_template(
            "authorize.jinja2",
            request,
            {
                "scope_text": scope_text,
                "client_name": client_name,
                "state": state,
                "response_type": response_type,
            },
        )

    await handle_oauth2_authorize(session, state, "yes")


@routes.post("/oauth/authorize")
@login_required
async def oauth2_authorize_post(request):
    session = await get_session(request)
    data = await request.post()

    await handle_oauth2_authorize(session, data.get("state"), data.get("confirm"))


async def handle_oauth2_authorize(session, state, confirm):
    userid = session["userid"]

    scopes = session["oauth2_authorize_scopes"]
    client_id = session["oauth2_authorize_client_id"]
    redirect_url = session["oauth2_authorize_redirect_url"]
    response_type = session["oauth2_response_type"]
    code_challenge = session["code_challenge"]
    code_challenge_method = session["code_challenge_method"]
    session_id = session.identity

    if not confirm or confirm != "yes":
        args = {"status": "error", "reason": "did_not_confirm"}

        del session["oauth2_authorize_scopes"]
        del session["oauth2_authorize_client_id"]
        del session["oauth2_authorize_redirect_url"]
        del session["oauth2_response_type"]
        del session["code_challenge"]
        del session["code_challenge_method"]

        await audit.create_audit_log_entry(
            "oauth",
            userid,
            "oauth_authorize",
            info={
                "client_id": client_id,
                "status": "error",
                "reason": "did_not_confirm",
            },
        )

        await error_redirect(redirect_url, args)

    valid, msg = await oauth.generate_authorization_response(
        userid,
        client_id,
        redirect_url,
        scopes,
        response_type,
        session_id=session_id,
        code_challenge=code_challenge,
        code_challenge_method=code_challenge_method,
    )

    if not valid:
        args = {"status": "error", "reason": msg, "state": state}

        del session["oauth2_authorize_scopes"]
        del session["oauth2_authorize_client_id"]
        del session["oauth2_authorize_redirect_url"]
        del session["oauth2_response_type"]
        del session["code_challenge"]
        del session["code_challenge_method"]

        await audit.create_audit_log_entry(
            "oauth",
            userid,
            "oauth_authorize",
            info={
                "client_id": client_id,
                "status": "error",
                "reason": msg,
                "state": state,
            },
        )

        await error_redirect(redirect_url, args)

    params = {"code": msg, "state": state}

    await audit.create_audit_log_entry(
        "oauth",
        userid,
        "oauth_authorize",
        info={"client_id": client_id, "status": "successful", "state": state},
    )

    raise web.HTTPSeeOther(urls.append_query(redirect_url, params))
