import datetime

from aiohttp import web
from aiohttp_session import get_session

from endpoints.middleware.authm import login_required

from api import admin, oauth

from util import errors

from db import audit

from security import hashing

routes = web.RouteTableDef()


async def check_api_key(api_key, scopes):
    if isinstance(scopes, str):
        scopes = [scopes]
    for s in scopes:
        if await admin.validate_api_key(api_key, s):
            return

    await errors.raise_unauthorized()


@routes.post("/api/admin/field")
async def admin_create_custom_field(request):
    data = await request.post()

    api_key = data.get("key")
    await check_api_key(api_key, "app_admin")

    name = data.get("name")
    display_name = data.get("display_name")
    include_with_scopes = data.get("include_with_scopes")

    if not name or not display_name or not include_with_scopes:
        await audit.create_audit_log_entry(
            "admin",
            await hashing.hash_sha256(api_key),
            "add_user_field",
            info={"name": name, "status": "error", "reason": "invalid_inputs"},
        )
        await errors.raise_bad_request({"status": "error", "message": "invalid_inputs"})

    await admin.create_custom_field(name, display_name, include_with_scopes)

    await audit.create_audit_log_entry(
        "admin",
        await hashing.hash_sha256(api_key),
        "add_user_field",
        info={"name": name, "status": "successful"},
    )

    return web.json_response({"status": "done"})


@routes.get("/impersonate/{userid}")
@login_required
async def admin_impersonate_user(request):
    userid = request.match_info["userid"]

    session = await get_session(request)

    data = await admin.impersonate_user(session["userid"], userid)

    if not data["successful"]:
        if data["error"] == "unauthorized":
            raise web.HTTPUnauthorized()
        elif data["error"] == "user_not_found":
            raise web.HTTPNotFound()
        raise web.HTTPBadRequest()

    session["impersonate_from_userid"] = session["userid"]
    session["userid"] = userid

    raise web.HTTPSeeOther("/")


@routes.get("/stop_impersonate")
@login_required
async def admin_stop_impersonating_user(request):
    session = await get_session(request)

    if "impersonate_from_userid" not in session:
        raise web.HTTPSeeOther("/")

    session["userid"] = session["impersonate_from_userid"]
    del session["impersonate_from_userid"]

    raise web.HTTPSeeOther("/")


@routes.get("/debug/token/userinfo")
@login_required
async def admin_debug_token_userinfo(request):
    session = await get_session(request)

    userid = session["userid"]

    data = await oauth.get_userinfo_by_userid(userid, "profile,forums")

    return web.json_response(data)


@routes.get("/time")
async def get_time(request):
    time = datetime.datetime.utcnow()

    return web.Response(body="{}Z".format(time.isoformat("T")))
