from aiohttp import web
from pydantic import ValidationError

from api import oauth
from db import audit
from models.OauthClient import OauthClientModel
from security import hashing
from util import errors, headers, urls

routes = web.RouteTableDef()


async def error_redirect(redirect_url, args):
    if not redirect_url:
        await errors.raise_bad_request(args)

    redirect_url = urls.append_query(redirect_url, args)

    raise web.HTTPSeeOther(redirect_url)


@routes.get("/api/oauth/userinfo")
async def oauth2_get_user(request):
    data = request.query
    bearer_token = data.get("bearer_token")
    if not bearer_token:
        if "Authorization" in request.headers:
            auth = request.headers["Authorization"]
            d = await headers.parse_authorization_header(auth)
            if "bearer_token" not in d:
                await errors.raise_bad_request({"error": "invalid_request"})
            bearer_token = d["bearer_token"]

    if not bearer_token:
        await errors.raise_bad_request(
            {"error": "invalid_request", "message": "no_bearer_token"}
        )

    d = await oauth.get_userinfo(bearer_token)

    if not d:
        await audit.create_audit_log_entry(
            "oauth",
            await hashing.hash_sha256(bearer_token),
            "token_userinfo",
            info={"status": "error", "reason": "invalid_bearer_token"},
        )
        await errors.raise_bad_request({"error": "invalid_bearer_token"})

    await audit.create_audit_log_entry(
        "oauth",
        await hashing.hash_sha256(bearer_token),
        "token_userinfo",
        info={"userid": d["userid"]},
    )

    return web.json_response(d)


@routes.get("/api/oauth/jwks")
async def oauth2_get_jwks(request):
    return web.json_response({"keys": [oauth.JWT_PUBKEY]})


@routes.post("/api/oauth/client/create")
async def oauth_create_client(request):
    bearer_token = await headers.get_bearer_token_from_request_headers(request)
    if not bearer_token:
        return web.Response(
            status=400, text="Bearer token is required in the Authorization headers"
        )

    user_info = await oauth.get_userinfo(bearer_token)

    try:
        sso_groups = user_info["groups"]
    except KeyError:
        return web.Response(status=400, text="Invalid or no groups")

    if sso_groups:
        groups = sso_groups.split(",")
        if "create_oauth_client" in groups:
            post_data = await request.json()
            try:
                oauth_client_data = OauthClientModel(  # noqa due to validating input data
                    email=post_data.get("email"),
                    domain=post_data.get("domain"),
                    name=post_data.get("name"),
                    redirect_uri=post_data.get("redirect_uri"),
                )
                oauth_client = await oauth.create_client(
                    post_data.get("email"),
                    post_data.get("domain"),
                    post_data.get("name"),
                    post_data.get("redirect_uri"),
                    False,
                    "profile",
                )
                return web.json_response(
                    {
                        "client_id": oauth_client.client_id,
                        "client_secret": oauth_client.client_secret,
                        "email": oauth_client.email,
                        "name": oauth_client.name,
                        "domain": oauth_client.domain,
                        "redirect_uri": oauth_client.redirect_uri,
                    },
                    status=201,
                )
            except ValidationError as e:
                print(f"Oauth Client model validation error: {e.errors()}")
                return web.json_response(e.errors(), status=400)
        else:
            return web.Response(status=403, text="Unauthorized access")

    return web.Response(status=400, text="Invalid request")
