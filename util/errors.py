import ujson

from aiohttp import web


async def raise_unauthorized():
    raise web.HTTPUnauthorized(
        body=ujson.dumps({"status": "error", "message": "unauthorized"}),
        content_type="application/json",
    )


async def raise_bad_request(body):
    raise web.HTTPBadRequest(body=ujson.dumps(body), content_type="application/json")
