import base64


async def parse_authorization_header(header):
    if header.startswith("Basic "):
        auth_header = header[len("Basic ") :]
        decoded_auth_header = base64.b64decode(auth_header).decode("UTF-8")
        colon_index = decoded_auth_header.index(":")
        client_id = decoded_auth_header[:colon_index]
        client_secret = decoded_auth_header[colon_index + 1 :]
        return {"client_id": client_id, "client_secret": client_secret}

    if header.startswith("Bearer "):
        auth_header = header[len("Bearer ") :]
        return {"bearer_token": auth_header}


async def get_bearer_token_from_request_headers(request):
    if "Authorization" in request.headers:
        auth = request.headers["Authorization"]
        d = await parse_authorization_header(auth)
        if "bearer_token" not in d:
            return False
        return d["bearer_token"]
    else:
        return False
