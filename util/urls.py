from requests.models import PreparedRequest


def append_query(url, params):
    req = PreparedRequest()
    req.prepare_url(url, params)

    return req.url
