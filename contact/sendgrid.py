from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail, Content


SENDGRID_API_KEY = None
SGCLIENT = None


async def init(sendgrid_api_key):
    global SENDGRID_API_KEY
    global SGCLIENT

    print("Initializing Sendgrid emails")

    SENDGRID_API_KEY = sendgrid_api_key
    SGCLIENT = SendGridAPIClient(SENDGRID_API_KEY)


async def send_email(html, to_address, subject, from_address, from_name):
    content = Content("text/html", html)
    mail = Mail(
        from_email=(from_address, from_name),
        to_emails=to_address,
        subject=subject,
        html_content=content,
    )

    response = SGCLIENT.send(mail)

    return response
