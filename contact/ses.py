import boto3

__AWS_ACCESS_KEY = None
__AWS_SECRET_KEY = None
__AWS_REGION = None

CLIENT = None


async def init(aws_access_key, aws_secret_key, aws_region):
    global __AWS_ACCESS_KEY
    global __AWS_SECRET_KEY
    global __AWS_REGION
    global CLIENT

    print("Initializing SES emails")

    __AWS_ACCESS_KEY = aws_access_key
    __AWS_SECRET_KEY = aws_secret_key
    __AWS_REGION = aws_region

    CLIENT = boto3.client(
        "ses",
        aws_access_key_id=__AWS_ACCESS_KEY,
        aws_secret_access_key=__AWS_SECRET_KEY,
        region_name=__AWS_REGION,
    )


async def send_email(content, to_address, to_name, subject, from_address, from_name):
    from_email = '"{}" <{}>'.format(from_name, from_address)
    to_email = '"{}" <{}>'.format(to_name, to_address)

    response = CLIENT.send_email(
        Source=from_email,
        Destination={
            "ToAddresses": [
                to_email,
            ]
        },
        Message={"Subject": {"Data": subject}, "Body": {"Html": {"Data": content}}},
    )

    if response["MessageId"]:
        return True

    return False
