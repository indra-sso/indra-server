from twilio.rest import Client


from serverconf import config


async def send_text(dest_user_number, contents):
    account_sid = await config.get_value("TWILIO_ACCOUNT_SID")
    auth_token = await config.get_value("TWILIO_ACCOUNT_TOKEN")
    src_number = await config.get_value("TWILIO_ACCOUNT_SRC_NUMBER")

    client = Client(account_sid, auth_token)

    client.messages.create(to=dest_user_number, from_=src_number, body=contents)


async def send_tfa_confirmation(dest_user_number, tfa_code):
    message = "Hello! Thanks for enabling two-factor authentication! Here's your code:\n{}".format(
        tfa_code
    )

    await send_text(dest_user_number, message)
