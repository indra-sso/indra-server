from jinja2 import Environment, FileSystemLoader, BaseLoader, select_autoescape


from contact import sendgrid, ses
from serverconf import config

from db import func as dbfunc, new_session

MAIL_TYPE = "sendgrid"
FROM_ADDRESS = "no-reply@sso.dte.io"
FROM_NAME = "DT SSO No-reply"


ENV = None
BENV = None


async def init(mail_type, from_address, from_name):
    global MAIL_TYPE
    global FROM_ADDRESS
    global FROM_NAME
    global ENV
    global BENV

    MAIL_TYPE = mail_type.lower()
    FROM_ADDRESS = from_address
    FROM_NAME = from_name

    if MAIL_TYPE == "sendgrid":
        await sendgrid.init(await config.get_value("SENDGRID_API_KEY"))
    elif MAIL_TYPE == "ses":
        await ses.init(
            await config.get_value("AWS_ACCESS_KEY_ID"),
            await config.get_value("AWS_SECRET_ACCESS_KEY"),
            await config.get_value("AWS_REGION"),
        )

    template_dir = await config.get_value("TEMPLATES_DIR")
    template_rel_dir = "templates/{}".format(template_dir)

    ENV = Environment(
        loader=FileSystemLoader(template_rel_dir),
        autoescape=select_autoescape(["html", "xml"]),
    )

    BENV = Environment(loader=BaseLoader())


async def send_email(content, to_address, to_name, subject):
    global MAIL_TYPE
    global FROM_ADDRESS
    global FROM_NAME
    global BENV

    session = new_session()

    email_obj = await dbfunc.create_email(
        session, to_address, to_name, FROM_ADDRESS, FROM_ADDRESS, subject, content
    )

    site_domain = await config.get_value("SITE_DOMAIN")
    view_email_url = "https://{}/email/view?token={}".format(
        site_domain, email_obj.view_token
    )

    template = BENV.from_string(content)
    content = template.render(view_email_url=view_email_url)

    if MAIL_TYPE == "sendgrid":
        return await sendgrid.send_email(
            content, to_address, subject, FROM_ADDRESS, FROM_NAME
        )
    if MAIL_TYPE == "ses":
        return await ses.send_email(
            content, to_address, to_name, subject, FROM_ADDRESS, FROM_NAME
        )
    else:
        raise ValueError("Unknown email API type")

    session.close()

    return False


async def send_verify_email(to_address, to_name, verify_token):
    global ENV
    template = ENV.get_template("email/verify.html")

    site_domain = await config.get_value("SITE_DOMAIN")

    business_address = await config.get_value("EMAIL_BUSINESS_ADDRESS")
    from_team_name = await config.get_value("EMAIL_FROM_TEAM_NAME")
    support_url = await config.get_value("SUPPORT_URL")
    site_url = await config.get_value("EMAIL_HOME_URL")
    logo_url = await config.get_value("EMAIL_LOGO_URL")

    verify_url = "https://{}/email/verify?token={}".format(site_domain, verify_token)

    content = template.render(
        business_address=business_address,
        from_team_name=from_team_name,
        support_url=support_url,
        verify_url=verify_url,
        site_url=site_url,
        logo_url=logo_url,
    )
    return await send_email(content, to_address, to_name, "Verify your email")


async def send_reset_email(to_address, to_name, reset_token):
    global ENV
    template = ENV.get_template("email/reset.html")

    site_domain = await config.get_value("SITE_DOMAIN")

    business_address = await config.get_value("EMAIL_BUSINESS_ADDRESS")
    from_team_name = await config.get_value("EMAIL_FROM_TEAM_NAME")
    support_url = await config.get_value("SUPPORT_URL")
    site_url = await config.get_value("EMAIL_HOME_URL")
    logo_url = await config.get_value("EMAIL_LOGO_URL")

    reset_url = "https://{}/email/reset?token={}".format(site_domain, reset_token)

    content = template.render(
        business_address=business_address,
        from_team_name=from_team_name,
        support_url=support_url,
        reset_url=reset_url,
        site_url=site_url,
        logo_url=logo_url,
    )
    return await send_email(content, to_address, to_name, "Reset your password")
