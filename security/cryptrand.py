import random
import secrets
import string

from ecdsa import SigningKey, NIST384p


async def generate_numeric_code(N):
    return "".join(random.choices(string.digits, k=N))


async def gen_crypto_chars(N):
    return secrets.token_urlsafe(N)


async def gen_oauth2_client_id():
    return await gen_crypto_chars(20)


async def gen_oauth2_client_secret():
    return await gen_crypto_chars(64)


async def gen_oauth2_access_token():
    return await gen_crypto_chars(64)


async def gen_oauth2_refresh_token():
    return await gen_crypto_chars(64)


async def generate_ecc_keys():
    sk = SigningKey.generate(curve=NIST384p)
    vk = sk.get_verifying_key()
    sk_pem = sk.to_pem()
    vk_pem = vk.to_pem()

    return vk_pem, sk_pem
