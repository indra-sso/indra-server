import hashlib
import base64
import ujson


async def hash_code_verifier(code_verifier, code_challenge_method):
    if code_challenge_method == "S256":
        return (
            base64.urlsafe_b64encode(
                hashlib.sha256(code_verifier.encode("UTF-8")).digest()
            )
            .decode("UTF-8")
            .replace("=", "")
        )

    return ""


async def hash_sha256(data):
    if isinstance(data, dict) or isinstance(data, list):
        data = ujson.dumps(data)
    if not isinstance(data, str):
        data = str(data)
    if isinstance(data, str):
        data = data.encode("UTF-8")
    return hashlib.sha256(data).hexdigest()
