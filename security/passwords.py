import base64

from passlib.hash import argon2

from security import ldappass


def hash_password(plaintext):
    return argon2.using(type="ID").hash(plaintext)


def verify_password(plaintext, data):
    try:
        b64d = base64.b64decode(data)
        ret = verify_ldap(plaintext, b64d)
        return ret, hash_password(plaintext)
    except:
        try:
            return argon2.using(type="ID").verify(plaintext, data), None
        except:
            return False, None


def verify_and_update(plaintext, data):
    valid, updated = verify_password(plaintext, data)
    if not valid:
        return False, None

    return True, updated


def verify_ldap(plaintext, data):
    return ldappass.check_password(data, plaintext)
