#!/usr/bin/env sh

set -e -u -x
set -o pipefail

cd repo

GIT_ID=$(git log -1 --pretty=format:'%h')
GIT_MSG=$(git log -1 --pretty=format:'%B')
GIT_AUTHOR=$(git log -1 --pretty=format:'%an')

echo "indra-server commit" \`$GIT_ID\` "(\"_"$GIT_MSG"_\" by "$GIT_AUTHOR") passed CI" >> ../message/passed
echo "indra-server commit" \`$GIT_ID\` "(\"_"$GIT_MSG"_\" by "$GIT_AUTHOR") failed CI" >> ../message/failed
echo "indra-server commit" \`$GIT_ID\` "(\"_"$GIT_MSG"_\" by "$GIT_AUTHOR") was aborted in CI" >> ../message/aborted

./tests/test.sh
