FROM posconsyam/centos-pipenv-8-3-11:latest

RUN python3.11 -m pip install --no-cache-dir pipenv

RUN dnf install -y nginx

RUN python3.11 -m pip install gunicorn

RUN mkdir -p /opt/gunicorn

COPY . /app
EXPOSE 80

WORKDIR /app

RUN pipenv sync

ENV PYTHONUNBUFFERED 1

ENTRYPOINT [ "/app/scripts/serve.sh" ]
