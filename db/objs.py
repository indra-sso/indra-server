import datetime

from sqlalchemy import Column, Integer, String, Boolean, DateTime
from sqlalchemy.orm.attributes import flag_modified
from sqlalchemy.dialects.postgresql import JSONB


from db.conf import Base
from db import conf

from security import passwords


class User(Base):
    __tablename__ = "user"
    __table_args__ = {"extend_existing": True}

    id = Column(Integer, primary_key=True)
    email = Column(String)
    password = Column(String)
    fname = Column(String)
    lname = Column(String)
    uname = Column(String)
    image = Column(String)
    tfa_enabled = Column(Boolean)
    tfa_avail = Column(String)
    tfa_otp_secret = Column(String)
    tfa_sms_phone_number = Column(String)
    tfa_sms_code = Column(String)
    email_verify_token = Column(String)
    email_reset_token = Column(String)
    verified = Column(Boolean)
    groups = Column(String)
    createdon = Column(DateTime)
    custom_fields = Column(JSONB)

    def __repr__(self):
        return "<User(id='%s', email='%s', uname='%s')>" % (
            str(self.id),
            str(self.email),
            str(self.uname),
        )

    def flag_modified_custom_fields(self):
        flag_modified(self, "custom_fields")

    def save(self, session=None):
        is_new_session = False
        if not session:
            is_new_session = True
            session = conf.new_session()

        if not self.createdon:
            self.createdon = datetime.datetime.utcnow()

        if self.id is None:
            session.add(self)

        try:
            return session.commit()
        except:
            session.rollback()
        finally:
            if is_new_session:
                session.close()

    def as_dict(self):
        d = {c.name: getattr(self, c.name) for c in self.__table__.columns}  # pylint: disable=no-member

        del d["password"]
        del d["tfa_otp_secret"]
        d["createdon"] = str(d["createdon"])

        return d

    def verify_password(self, password):
        ret, newpass = passwords.verify_and_update(password, self.password)

        if not ret:
            return False

        if newpass:
            self.password = newpass
            self.save()

        return True


class Email(Base):
    __tablename__ = "email"
    __table_args__ = {"extend_existing": True}

    id = Column(Integer, primary_key=True)
    to_address = Column(String)
    to_name = Column(String)
    from_address = Column(String)
    from_name = Column(String)
    content = Column(String)
    subject = Column(String)
    view_token = Column(String)
    senton = Column(String)

    def __repr__(self):
        return "<Email(id='%s', to_address='%s', to_name='%s')>" % (
            str(self.id),
            str(self.to_address),
            str(self.to_name),
        )

    def save(self, session=None):
        is_new_session = False
        if not session:
            is_new_session = True
            session = conf.new_session()

        if self.id is None:
            session.add(self)

        try:
            return session.commit()
        except:
            session.rollback()
        finally:
            if is_new_session:
                session.close()

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}  # pylint: disable=no-member


class APIKey(Base):
    __tablename__ = "apikey"
    __table_args__ = {"extend_existing": True}

    id = Column(Integer, primary_key=True)
    key = Column(String)
    scope = Column(String)
    createdon = Column(DateTime)
    valid = Column(Boolean)

    def __repr__(self):
        return "<APIKey(id='%s', key='%s', scope='%s')>" % (
            str(self.id),
            str(self.key),
            str(self.scope),
        )

    def save(self, session=None):
        is_new_session = False
        if not session:
            is_new_session = True
            session = conf.new_session()

        if not self.createdon:
            self.createdon = datetime.datetime.utcnow()

        if self.id is None:
            session.add(self)

        try:
            return session.commit()
        except:
            session.rollback()
        finally:
            if is_new_session:
                session.close()

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}  # pylint: disable=no-member


class ConfOption(Base):
    __tablename__ = "confoption"
    __table_args__ = {"extend_existing": True}

    id = Column(Integer, primary_key=True)
    name = Column(String)
    value = Column(String)
    createdon = Column(DateTime)
    updatedon = Column(DateTime)

    def __repr__(self):
        return "<ConfOption(id='%s', name='%s', updatedon='%s')>" % (
            str(self.id),
            str(self.name),
            str(self.updatedon),
        )

    def save(self, session=None):
        is_new_session = False
        if not session:
            is_new_session = True
            session = conf.new_session()

        if not self.createdon:
            self.createdon = datetime.datetime.utcnow()
        self.updatedon = datetime.datetime.utcnow()

        if self.id is None:
            session.add(self)

        try:
            return session.commit()
        except:
            session.rollback()
        finally:
            if is_new_session:
                session.close()

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}  # pylint: disable=no-member


class OAuth2Client(Base):
    __tablename__ = "oauthclient"
    __table_args__ = {"extend_existing": True}

    id = Column(Integer, primary_key=True)
    email = Column(String)
    domain = Column(String)
    client_id = Column(String)
    client_secret = Column(String)
    name = Column(String)
    redirect_uri = Column(String)
    granttype = Column(String)
    response_type = Column(String)
    scopes = Column(String)
    must_authorize = Column(Boolean)

    def __repr__(self):
        return "<OAuthClient(id='%s', email='%s', domain='%s')>" % (
            str(self.id),
            str(self.email),
            str(self.domain),
        )

    def save(self, session=None):
        is_new_session = False
        if not session:
            is_new_session = True
            session = conf.new_session()

        if self.id is None:
            session.add(self)

        try:
            return session.commit()
        except:
            session.rollback()
        finally:
            if is_new_session:
                session.close()

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}  # pylint: disable=no-member


class OAuth2BearerToken(Base):
    __tablename__ = "bearertoken"
    __table_args__ = {"extend_existing": True}

    id = Column(Integer, primary_key=True)
    userid = Column(String)
    scopes = Column(String)
    client_id = Column(String)
    access_token = Column(String)
    refresh_token = Column(String)
    expiration_time = Column(DateTime)
    createdon = Column(DateTime)
    session_id = Column(String)
    valid = Column(Boolean)
    aud = Column(String)

    def __repr__(self):
        return "<BearerToken(id='%s', userid='%s', client_id='%s')>" % (
            str(self.id),
            str(self.userid),
            str(self.client_id),
        )

    def save(self, session=None):
        is_new_session = False
        if not session:
            is_new_session = True
            session = conf.new_session()

        if self.id is None:
            session.add(self)
        if not self.createdon:
            self.createdon = datetime.datetime.utcnow()

        try:
            return session.commit()
        except:
            session.rollback()
        finally:
            if is_new_session:
                session.close()

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}  # pylint: disable=no-member


class OAuth2AuthorizationCode(Base):
    __tablename__ = "authorizationcode"
    __table_args__ = {"extend_existing": True}

    id = Column(Integer, primary_key=True)
    client_id = Column(String)
    userid = Column(String)
    scopes = Column(String)
    redirect_url = Column(String)
    authorization_code = Column(String)
    expiration_time = Column(DateTime)
    valid = Column(Boolean)
    createdon = Column(DateTime)
    session_id = Column(String)

    def __repr__(self):
        return "<AuthorizationCode(id='%s', userid='%s', authorization_code='%s')>" % (
            str(self.id),
            str(self.userid),
            str(self.authorization_code),
        )

    def save(self, session=None):
        is_new_session = False
        if not session:
            is_new_session = True
            session = conf.new_session()

        if self.id is None:
            session.add(self)
        if not self.createdon:
            self.createdon = datetime.datetime.utcnow()

        try:
            return session.commit()
        except:
            session.rollback()
        finally:
            if is_new_session:
                session.close()

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}  # pylint: disable=no-member

    def is_valid(self, client_id):
        if not self.valid:
            return False

        if client_id != self.client_id:
            return False

        if self.expiration_time < datetime.datetime.utcnow():
            return False

        return True


class OAuth2PKCECode(Base):
    __tablename__ = "pkcecode"
    __table_args__ = {"extend_existing": True}

    id = Column(Integer, primary_key=True)
    client_id = Column(String)
    userid = Column(String)
    scopes = Column(String)
    redirect_url = Column(String)
    hashed_code_verifier = Column(String)
    code_challenge_method = Column(String)
    code = Column(String)
    valid = Column(Boolean)
    createdon = Column(DateTime)
    session_id = Column(String)

    def __repr__(self):
        return "<PKCECode(id='%s', userid='%s', hashed_code_verifier='%s')>" % (
            str(self.id),
            str(self.userid),
            str(self.hashed_code_verifier),
        )

    def save(self, session=None):
        is_new_session = False
        if not session:
            is_new_session = True
            session = conf.new_session()

        if self.id is None:
            session.add(self)
        if not self.createdon:
            self.createdon = datetime.datetime.utcnow()

        try:
            return session.commit()
        except:
            session.rollback()
        finally:
            if is_new_session:
                session.close()

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}  # pylint: disable=no-member


class AuditLogEntry(Base):
    __tablename__ = "auditlog"
    __table_args__ = {"extend_existing": True}

    id = Column(Integer, primary_key=True)
    bytype = Column(String)
    by = Column(String)
    action = Column(String)
    date = Column(DateTime)
    signature = Column(String)
    info = Column(String)

    def __repr__(self):
        return (
            "<AuditLogEntry(id='%s', bytype='%s', by='%s', action='%s', signature='%s')>"
            % (
                str(self.id),
                str(self.bytype),
                str(self.by),
                str(self.action),
                str(self.signature),
            )
        )

    def save(self, session=None):
        is_new_session = False
        if not session:
            is_new_session = True
            session = conf.new_session()

        if self.id is None:
            session.add(self)
        if not self.date:
            self.date = datetime.datetime.utcnow()

        try:
            return session.commit()
        except:
            session.rollback()
        finally:
            if is_new_session:
                session.close()

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}  # pylint: disable=no-member


class CustomField(Base):
    __tablename__ = "customfield"
    __table_args__ = {"extend_existing": True}

    id = Column(Integer, primary_key=True)
    programmatic_name = Column(String)
    display_name = Column(String)
    include_with_scopes = Column(String)

    def __repr__(self):
        return "<CustomField(id='%s', programmatic_name='%s', display_name='%s'>" % (
            str(self.id),
            str(self.programmatic_name),
            str(self.display_name),
        )

    def save(self, session=None):
        is_new_session = False
        if not session:
            is_new_session = True
            session = conf.new_session()

        if self.id is None:
            session.add(self)

        try:
            return session.commit()
        except:
            session.rollback()
        finally:
            if is_new_session:
                session.close()

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}  # pylint: disable=no-member
