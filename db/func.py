from sqlalchemy import func as sqlfunc
from sqlalchemy import or_, text

from db import conf, objs
from security import cryptrand


async def create_user(session, email, password, fname, lname, uname):
    e = objs.User(email=email, password=password, fname=fname, lname=lname, uname=uname)

    e.email_verify_token = await cryptrand.gen_crypto_chars(16)

    e.save(session)

    return e


async def get_all_users(session, limit=100, offset=0, order_by=None, **kwargs):
    try:
        q = session.query(objs.User).filter_by(**kwargs)
        if order_by:
            col_name = order_by[1:]
            direction = order_by[0]
            a = getattr(objs.User, col_name)
            if direction == "+":
                q = q.order_by(a.asc())
            elif direction == "-":
                q = q.order_by(a.desc())
        return q.limit(limit).offset(offset)
    except Exception as e:
        session.rollback()
        print(e)


async def get_user(session, **kwargs):
    try:
        users = await get_all_users(session, **kwargs)
        return users.first()
    except Exception as e:
        session.rollback()
        print(e)


async def get_user_by_email(session, email):
    try:
        return (
            session.query(objs.User)
            .filter(sqlfunc.lower(objs.User.email) == sqlfunc.lower(email))
            .first()
        )
    except Exception as e:
        session.rollback()
        print(e)


async def get_user_by_uname(session, uname):
    try:
        return (
            session.query(objs.User)
            .filter(sqlfunc.lower(objs.User.uname) == sqlfunc.lower(uname))
            .first()
        )
    except Exception as e:
        session.rollback()
        print(e)


async def get_user_by_fields(session, fields, limit=100, offset=0):
    try:
        user_objs = session.query(objs.User)

        for field in fields:
            user_objs = user_objs.filter(
                objs.User.custom_fields[field].astext == fields[field]
            )

        return user_objs.limit(limit).offset(offset)
    except Exception as e:
        session.rollback()
        print(e)


async def get_user_by_groups(session, groups, limit=100, offset=0, order_by=None):
    try:
        sql = """SELECT id 
        FROM public."user" WHERE 
        """

        groups_dict = {}

        for group_id in range(len(groups)):
            group_id_name = "group_{}".format(group_id)

            groups_dict[group_id_name] = groups[group_id]

            if group_id > 0:
                sql += " OR "
            # NOTE: There's a possibility of SQL injection here
            sql += """:{group_id_name} = any(regexp_split_to_array(public."user".groups, ','))""".format(
                group_id_name=group_id_name
            )

        sql += ";"

        with conf.engine.connect() as con:
            res = con.execute(text(sql), **groups_dict)

        id_arr = [r.id for r in res]

        q = session.query(objs.User).filter(objs.User.id.in_(id_arr))

        if order_by:
            col_name = order_by[1:]
            direction = order_by[0]
            a = getattr(objs.User, col_name)
            if direction == "+":
                q = q.order_by(a.asc())
            elif direction == "-":
                q = q.order_by(a.desc())
        total = q.count()
        return q.limit(limit).offset(offset).all(), total

    except Exception as e:
        session.rollback()
        print(e)


async def create_oauth2client(
    session, email, domain, name, redirect_uri, must_authorize=True, scopes=None
):
    e = objs.OAuth2Client(
        email=email,
        domain=domain,
        name=name,
        redirect_uri=redirect_uri,
        must_authorize=must_authorize,
    )

    e.client_id = await cryptrand.gen_oauth2_client_id()
    e.client_secret = await cryptrand.gen_oauth2_client_secret()

    if scopes is not None:
        e.scopes = scopes

    e.save(session)

    return e


async def get_oauth2client(session, **kwargs):
    try:
        return session.query(objs.OAuth2Client).filter_by(**kwargs).first()
    except Exception as e:
        session.rollback()
        print(e)


async def create_oauth2authorizationcode(
    session,
    userid,
    client_id,
    redirect_url,
    scopes,
    authorization_code,
    exp,
    session_id=None,
):
    e = objs.OAuth2AuthorizationCode(
        userid=userid,
        client_id=client_id,
        redirect_url=redirect_url,
        scopes=scopes,
        authorization_code=authorization_code,
        valid=True,
        expiration_time=exp,
        session_id=session_id,
    )

    e.save(session)

    return e


async def get_oauth2authorizationcode(session, **kwargs):
    try:
        return (await get_all_oauth2authorizationcode(session, **kwargs)).first()
    except Exception as e:
        print(e)


async def get_all_oauth2authorizationcode(session, **kwargs):
    try:
        return session.query(objs.OAuth2AuthorizationCode).filter_by(**kwargs)
    except Exception as e:
        session.rollback()
        print(e)


async def create_oauth2pkcecode(
    session,
    userid,
    client_id,
    redirect_url,
    scopes,
    code_challenge,
    code_challenge_method,
    session_id=None,
):
    code = await cryptrand.gen_crypto_chars(16)

    e = objs.OAuth2PKCECode(
        userid=userid,
        client_id=client_id,
        redirect_url=redirect_url,
        scopes=scopes,
        code=code,
        valid=True,
        hashed_code_verifier=code_challenge,
        code_challenge_method=code_challenge_method,
        session_id=session_id,
    )
    e.save(session)

    return e


async def get_oauth2pkcecode(session, **kwargs):
    try:
        return (await get_all_oauth2pkcecode(session, **kwargs)).first()
    except Exception as e:
        session.rollback()
        print(e)


async def get_all_oauth2pkcecode(session, **kwargs):
    try:
        return session.query(objs.OAuth2PKCECode).filter_by(**kwargs)
    except Exception as e:
        session.rollback()
        print(e)


async def get_oauth2bearertoken(session, **kwargs):
    try:
        return (await get_all_oauth2bearertoken(session, **kwargs)).first()
    except Exception as e:
        session.rollback()
        print(e)


async def get_all_oauth2bearertoken(session, **kwargs):
    try:
        return session.query(objs.OAuth2BearerToken).filter_by(**kwargs)
    except Exception as e:
        session.rollback()
        print(e)


async def create_oauth2bearertoken(
    session, userid, client_id, scopes, exp, session_id=None, aud=None
):
    access_token = await cryptrand.gen_oauth2_access_token()
    refresh_token = await cryptrand.gen_oauth2_access_token()

    e = objs.OAuth2BearerToken(
        userid=userid,
        client_id=client_id,
        access_token=access_token,
        refresh_token=refresh_token,
        scopes=scopes,
        expiration_time=exp,
        session_id=session_id,
        valid=True,
        aud=aud,
    )

    e.save(session)

    return e


async def create_email(
    session, to_address, to_name, from_address, from_name, subject, content
):
    view_token = await cryptrand.gen_crypto_chars(16)

    e = objs.Email(
        to_address=to_address,
        to_name=to_name,
        from_address=from_address,
        from_name=from_name,
        subject=subject,
        content=content,
        view_token=view_token,
    )

    e.save(session)

    return e


async def create_confoption(session, name, value):
    e = objs.ConfOption(name=name, value=value)

    e.save(session)

    return e


async def get_confoption(session, **kwargs):
    try:
        return session.query(objs.ConfOption).filter_by(**kwargs).first()
    except Exception as e:
        session.rollback()
        print(e)


async def create_apikey(scope, session=None):
    key = await cryptrand.gen_crypto_chars(32)
    e = objs.APIKey(key=key, valid=True, scope=scope)

    if session:
        e.save(session)

    return e


async def get_apikey(session, **kwargs):
    try:
        return session.query(objs.APIKey).filter_by(**kwargs).first()
    except Exception as e:
        session.rollback()
        print(e)


async def invalidate_oauth2_pkcecode(session, session_id):
    db_objs = await get_all_oauth2pkcecode(session, session_id=session_id)

    for obj in db_objs:
        obj.valid = False
        obj.save(session)


async def invalidate_oauth2_authorizationcode(session, session_id):
    db_objs = await get_all_oauth2authorizationcode(session, session_id=session_id)

    for obj in db_objs:
        obj.valid = False
        obj.save(session)


async def invalidate_oauth2_bearertoken(session, session_id):
    db_objs = await get_all_oauth2bearertoken(session, session_id=session_id)

    for obj in db_objs:
        obj.valid = False
        obj.save(session)


async def get_all_audit_log_entry(session):
    try:
        return session.query(objs.AuditLogEntry)
    except Exception as e:
        session.rollback()
        print(e)


async def get_audit_log_entry(session, **kwargs):
    try:
        return (await get_all_audit_log_entry(session)).filter_by(**kwargs).first()
    except Exception as e:
        session.rollback()
        print(e)


async def get_latest_audit_log_entry(session):
    try:
        query = await get_all_audit_log_entry(session)

        query = query.order_by(objs.AuditLogEntry.id.desc())

        return query.first()
    except Exception as e:
        session.rollback()
        print(e)


async def get_first_audit_log_entry(session):
    try:
        query = await get_all_audit_log_entry(session)

        query = query.order_by(objs.AuditLogEntry.id.asc())

        return query.first()
    except Exception as e:
        session.rollback()
        print(e)


async def create_custom_field(session, name, display_name, include_with_scopes):
    e = objs.CustomField(
        programmatic_name=name,
        display_name=display_name,
        include_with_scopes=include_with_scopes,
    )

    e.save(session)

    return e


async def get_all_custom_field(session, **kwargs):
    try:
        return session.query(objs.CustomField).filter_by(**kwargs)
    except Exception as e:
        session.rollback()
        print(e)


async def get_custom_field(session, **kwargs):
    try:
        out = await get_all_custom_field(**kwargs)
        return out.first()
    except Exception as e:
        session.rollback()
        print(e)


async def search_user(session, query, limit=100, offset=0, order_by=None):
    try:
        q_int = int(query)
    except:
        q_int = 0
    q = "%{}%".format(query)
    try:
        q = session.query(objs.User).filter(
            or_(
                sqlfunc.lower(objs.User.fname).like(q),
                sqlfunc.lower(objs.User.lname).like(q),
                sqlfunc.lower(objs.User.email).like(q),
                objs.User.id == q_int,
            )
        )

        if order_by:
            col_name = order_by[1:]
            direction = order_by[0]
            a = getattr(objs.User, col_name)
            if direction == "+":
                q = q.order_by(a.asc())
            elif direction == "-":
                q = q.order_by(a.desc())

        total = q.count()
        return q.limit(limit).offset(offset).all(), total
    except Exception as e:
        session.rollback()
        print(e)
