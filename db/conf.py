import os

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base


engine = create_engine(os.environ.get("POSTGRES_HOST"), echo="debug")

Base = declarative_base()
SessionMaker = None


def new_session():
    return SessionMaker()


def init():
    global session
    global SessionMaker
    print("Initializing database")
    Base.metadata.create_all(engine)

    # https://docs.sqlalchemy.org/en/20/orm/session_basics.html
    # for "Web applications" it says that "It’s also usually a good idea to set Session.expire_on_commit to False
    # so that subsequent access to objects that came from a Session within the view layer do not need to emit new
    # SQL queries to refresh the objects, if the transaction has been committed already."
    SessionMaker = sessionmaker(bind=engine, expire_on_commit=False)
