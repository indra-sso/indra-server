import ujson

from db import objs, conf, func
from db.conf import new_session


from security import hashing


async def create_audit_log_entry(bytype, by, action, info=None, session=None):
    try:
        if not session:
            is_new_session = True
            session = new_session()

        new_obj = objs.AuditLogEntry(
            bytype=str(bytype), by=str(by), action=str(action), info=ujson.dumps(info)
        )

        new_obj.save(session)

        obj = await func.get_audit_log_entry(session, id=new_obj.id)
        return obj.signature
    finally:
        if is_new_session:
            session.close()


async def create_signature(last_obj, current_obj, session):
    current_signature = await calculate_signature(last_obj.signature, current_obj)

    current_obj.signature = current_signature

    current_obj.save(session)


async def calculate_signature(last_signature, current_obj):
    current_dict = current_obj.as_dict()
    del current_dict["signature"]
    current_dict["date"] = current_dict["date"].isoformat()
    current_json = ujson.dumps(current_dict)

    current_hash = await hashing.hash_sha256(current_json)
    hashstr = "{}_{}".format(last_signature, current_hash)
    current_signature = await hashing.hash_sha256(hashstr)

    return current_signature


async def create_audit_log_genesis():
    session = new_session()

    session.begin_nested()

    session.execute("LOCK TABLE auditlog IN ACCESS EXCLUSIVE MODE;")

    test_objs = await func.get_audit_log_entry()
    assert not test_objs

    first_obj = objs.AuditLogEntry(
        bytype="genesis", by="genesis", action="Generate genesis block"
    )
    first_obj.save()

    obj = await func.get_audit_log_entry(id=first_obj.id)

    sig = await calculate_signature("", obj)

    obj.signature = sig
    obj.save()

    conf.session.commit()


async def verify_signature_chain():
    first_obj = await func.get_first_audit_log_entry()

    print("Checking {}...".format(first_obj.id), end="")
    sig = await calculate_signature("", first_obj)
    if sig != first_obj.signature:
        print("Error")
    else:
        print("Checked")

    last_obj = first_obj
    check_id = 1000
    while True:
        obj = await func.get_audit_log_entry(id=check_id)
        if not obj:
            break
        print("Checking {}...".format(obj.id), end="")
        sig = await calculate_signature(last_obj.signature, obj)
        if sig != obj.signature:
            print("Error")
        else:
            print("Checked")
        last_obj = obj
        check_id += 1

    return True
