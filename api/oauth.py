import asyncio
import datetime
import os
import time

import jwt
import ujson
from jwcrypto.jwk import JWK

from db import func as dbfunc
from db import new_session
from security import cryptrand, hashing
from serverconf import config

JWT_SECRET = None
JWT_PUBKEY = None
EXPIRE_TIME = 3600

BUG_TOKEN_INSECURE = os.environ.get("BUG_TOKEN_INSECURE") == "true"


async def generate_authorization_response(
    userid,
    client_id,
    redirect_url,
    scopes,
    response_type,
    session_id=None,
    code_challenge=None,
    code_challenge_method=None,
    session=None,
):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()
        client = await get_client(client_id, session=session)

        if not client:
            return False, "invalid_client_id_generate_auth_response"

        valid_scopes, err = await validate_scopes(
            client_id, scopes, client=client, session=session
        )

        if not valid_scopes:
            return False, err

        if not scopes:
            scopes = await get_scopes(client_id, client=client, session=session)

        if response_type == "code":
            if not code_challenge:
                authorization_code = await create_authorization_code(
                    userid,
                    client_id,
                    redirect_url,
                    scopes,
                    session_id=session_id,
                    session=session,
                )
            else:
                authorization_code = await create_pkce_code(
                    userid,
                    client_id,
                    redirect_url,
                    scopes,
                    code_challenge,
                    code_challenge_method,
                    session_id=session_id,
                    session=session,
                )
        else:
            return False, "invalid_response_type"
        return True, authorization_code
    finally:
        if is_new_session:
            session.close()


async def create_authorization_code(
    userid, client_id, redirect_url, scopes, session_id=None, session=None
):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()
        now = datetime.datetime.utcnow()
        exp = now + datetime.timedelta(seconds=120)

        if isinstance(scopes, list):
            scopes = ",".join(scopes)

        payload = {
            "userid": userid,
            "client_id": client_id,
            "redirect_url": redirect_url,
            "scope": scopes,
            "iss": int(now.timestamp()),
            "exp": int(exp.timestamp()),
        }

        authorization_code = jwt.encode(payload, JWT_SECRET, algorithm="ES256")

        await dbfunc.create_oauth2authorizationcode(
            session,
            userid,
            client_id,
            redirect_url,
            scopes,
            authorization_code,
            exp,
            session_id=session_id,
        )

        return authorization_code
    finally:
        if is_new_session:
            session.close()


async def create_pkce_code(
    userid,
    client_id,
    redirect_url,
    scopes,
    code_challenge,
    code_challenge_method,
    session_id=None,
    session=None,
):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()
        if isinstance(scopes, list):
            scopes = ",".join(scopes)

        code_obj = await dbfunc.create_oauth2pkcecode(
            session,
            userid,
            client_id,
            redirect_url,
            scopes,
            code_challenge,
            code_challenge_method,
            session_id=session_id,
        )

        return code_obj.code
    finally:
        if is_new_session:
            session.close()


async def verify_pkce_code(code, code_verifier, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()
        code_obj = await dbfunc.get_oauth2pkcecode(session, code=code)

        if not code_obj.valid:
            return None

        hashed_code_verifier = await hashing.hash_code_verifier(
            code_verifier, code_obj.code_challenge_method
        )

        if hashed_code_verifier != code_obj.hashed_code_verifier:
            return None

        exp = datetime.datetime.utcnow() + datetime.timedelta(seconds=EXPIRE_TIME)
        scopes = code_obj.scopes
        if isinstance(scopes, list):
            scopes = ",".join(scopes)

        token_obj = await dbfunc.create_oauth2bearertoken(
            session,
            code_obj.userid,
            code_obj.client_id,
            scopes,
            exp,
            session_id=code_obj.session_id,
        )

        access_token = token_obj.access_token
        refresh_token = token_obj.refresh_token

        code_obj.valid = False
        code_obj.save(session)
        expire_time_generated = int(exp.strftime("%S"))

        d = {
            "access_token": access_token,
            "refresh_token": refresh_token,
            "expires": EXPIRE_TIME,
            "expires_at": expire_time_generated,
            "token_type": "bearer",
            "scope": scopes,
            "id_token": await gen_id_token(
                access_token, code_obj.userid, code_obj.client_id, session=session
            ),
        }

        return d
    finally:
        if is_new_session:
            session.close()


async def exchange_authorization_code(
    authorization_code, client_id, client_secret, aud=None, session=None
):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()
        client = await get_client_with_secret(
            client_secret, client_id=client_id, session=session
        )

        if not client:
            return None, "invalid_client_id_exch_auth_code"

        code_obj = await dbfunc.get_oauth2authorizationcode(
            session, authorization_code=authorization_code
        )

        if not code_obj or not code_obj.is_valid(client_id):
            return None, "invalid_authorization_code"

        code_obj.valid = False

        code_obj.save(session)

        access_token = await create_access_token(
            code_obj.userid,
            client_id,
            code_obj.scopes,
            session_id=code_obj.session_id,
            aud=aud,
            session=session,
        )
        return access_token, ""
    finally:
        if is_new_session:
            session.close()


async def create_access_token(
    userid, client_id, scopes, session_id=None, aud=None, session=None
):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()
        exp = datetime.datetime.utcnow() + datetime.timedelta(seconds=EXPIRE_TIME)
        if isinstance(scopes, list):
            scopes = ",".join(scopes)

        token_obj = await dbfunc.create_oauth2bearertoken(
            session, userid, client_id, scopes, exp, session_id=session_id, aud=aud
        )

        access_token = token_obj.access_token
        refresh_token = token_obj.refresh_token

        expire_time_generated = int(exp.strftime("%S"))

        d = {
            "access_token": access_token,
            "refresh_token": refresh_token,
            "expires": EXPIRE_TIME,
            "expires_at": expire_time_generated,
            "token_type": "bearer",
            "scope": scopes,
            "id_token": await gen_id_token(
                access_token, userid, client_id, session=session
            ),
        }

        return d
    finally:
        if is_new_session:
            session.close()


async def refresh_bearer_token(refresh_token, client_id, client_secret, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()
        bearertoken = await get_bearer_from_refresh(refresh_token, session=session)

        if not bearertoken:
            return None, "no_bearer_token"

        if bearertoken.client_id != client_id:
            return None, "client_id_mismatch"

        if not bearertoken.valid:
            return None, "bearer_token_invalid"

        if client_secret:
            if not await validate_client_id(
                client_id, client_secret=client_secret, session=session
            ):
                return None, "invalid_client_secret"

        bearertoken.valid = False
        bearertoken.save(session)

        return await create_access_token(
            bearertoken.userid,
            client_id,
            bearertoken.scopes,
            session_id=bearertoken.session_id,
            session=session,
        ), ""
    finally:
        if is_new_session:
            session.close()


async def handle_token_post(data, client_id=None, client_secret=None):
    audit_log_type = "token_post"
    error_msg = ""
    access_token = None

    grant_type = data.get("grant_type")
    code = data.get("code")
    code_verifier = data.get("code_verifier")
    refresh_token = data.get("refresh_token")

    if grant_type == "authorization_code":
        if code_verifier:
            audit_log_type += "_pkce"
            access_token, error_msg = await token_post_authorization_code_pkce(
                code, code_verifier
            )
        else:
            audit_log_type += "_exchange_authorization_code"
            access_token, error_msg = await token_post_authorization_code(
                code, client_id, client_secret
            )

    elif grant_type == "refresh_token":
        audit_log_type += "_refresh_authorization_code"
        access_token, error_msg = await token_post_refresh_bearer_token(
            refresh_token, client_id, client_secret
        )

    elif grant_type == "client_credentials":
        audit_log_type += "_client_credentials"
        access_token, error_msg = await token_post_client_credentials(
            client_id, client_secret
        )
    else:
        error_msg = "invalid_grant_type"

    return access_token, audit_log_type, error_msg


async def token_post_authorization_code(code, client_id, client_secret):
    if not client_id:
        return None, "invalid_client_id_token_post_auth_code"
    if not client_secret:
        return None, "invalid_client_secret"
    if not code:
        return None, "invalid_authorization_code"

    access_token, error_msg = await exchange_authorization_code(
        code, client_id, client_secret
    )
    return access_token, error_msg


async def token_post_authorization_code_pkce(code, code_verifier):
    if not code:
        return None, "invalid_authorization_code"
    if not code_verifier:
        return None, "invalid_code_verifier"

    access_token = await verify_pkce_code(code, code_verifier)
    return access_token, ""


async def token_post_refresh_bearer_token(refresh_token, client_id, client_secret):
    if not client_id:
        return None, "invalid_client_id_token_post_refresh_bearer"
    if not refresh_token:
        return None, "invalid_refresh_token"

    access_token, error_msg = await refresh_bearer_token(
        refresh_token, client_id, client_secret
    )
    return access_token, error_msg


async def token_post_client_credentials(client_id, client_secret):
    if not client_id:
        return None, "invalid_client_id_token_post_client_creds"
    if not client_secret:
        return None, "invalid_client_secret"

    client = await get_client_with_secret(client_secret, client_id)
    if not client:
        return None, "invalid_client_credentials"

    access_token = await create_access_token(client_id, client_id, ["openid"])
    return access_token, ""


async def gen_id_token(access_token, sub, aud, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()
        payload = await get_userinfo(access_token, session=session)
        if payload is None:
            return None
        payload["iss"] = "https://{}".format(await config.get_value("SITE_DOMAIN"))
        payload["iat"] = int(time.time())
        payload["sub"] = sub
        payload["exp"] = payload["iat"] + EXPIRE_TIME
        payload["aud"] = aud

        return jwt.encode(payload, JWT_SECRET, algorithm="ES384")
    finally:
        if is_new_session:
            session.close()


async def get_token_info(access_token, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()
        token_obj = await dbfunc.get_oauth2bearertoken(
            session, access_token=access_token
        )
        if not token_obj:
            return {"message": "invalid token."}

        if not token_obj.valid:
            return {"message": "token is not valid."}

        if token_obj.expiration_time <= datetime.datetime.utcnow():
            return {"message": "token expired."}

        d = {
            "expires_in": (
                token_obj.expiration_time - datetime.datetime.utcnow()
            ).seconds,
            "scope": token_obj.scopes,
            "token_type": "bearer",
            "active": token_obj.valid
            and token_obj.expiration_time > datetime.datetime.utcnow(),
        }

        return d
    finally:
        if is_new_session:
            session.close()


async def get_userinfo(bearer_token, scopes=None, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()
        if not scopes:
            scopes = await get_scopes_from_bearer_token(bearer_token, session=session)

        user = await get_user_from_bearer_token(bearer_token, session=session)
        if not user:
            return {}

        return await get_userinfo_by_userid(user.id, scopes, user=user, session=session)
    finally:
        if is_new_session:
            session.close()


async def get_userinfo_by_userid(userid, scopes, user=None, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()
        if not user:
            user = await dbfunc.get_user(session, id=userid)

        if not user:
            return {}

        if isinstance(scopes, str):
            if "," in scopes:
                scopes = scopes.split(",")
            else:
                scopes = scopes.split(" ")

        out = {"userid": user.id}
        if "profile" in scopes:
            out["name"] = user.uname
            out["given_name"] = user.fname
            out["family_name"] = user.lname
            out["email"] = user.email
            out["groups"] = user.groups
            out["image"] = await get_user_image_url(user.image)
        else:
            if "email" in scopes:
                out["email"] = user.email
            if "groups" in scopes:
                out["groups"] = user.groups
            if "full_name" in scopes:
                out["given_name"] = user.fname
                out["family_name"] = user.lname
            elif "uname" in scopes:
                out["name"] = user.uname
            if "image" in scopes:
                out["image"] = await get_user_image_url(user.image)

        for scope in scopes:
            if scope.startswith("field."):
                scope = scope[len("field.") :]
                if scope in user.custom_fields:
                    out[scope] = user.custom_fields[scope]
            else:
                scope_objs = await dbfunc.get_all_custom_field(
                    session, include_with_scopes=scope
                )
                for obj in scope_objs.all():
                    if not obj:
                        continue
                    if obj.programmatic_name in user.custom_fields:
                        out[obj.programmatic_name] = user.custom_fields[
                            obj.programmatic_name
                        ]

        return out
    finally:
        if is_new_session:
            session.close()


async def get_user_image_url(image):
    imageurl = await config.get_value("PROFILE_IMAGE_URL_BASE")
    if image:
        imageurl = os.path.join(imageurl, image)
    else:
        imageurl = os.path.join(
            imageurl, await config.get_value("PROFILE_DEFAULT_IMAGE")
        )
    return imageurl


async def get_user_from_bearer_token(
    bearer_token, requesting_client_id=None, session=None
):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()
        token_obj = await get_bearer_token(bearer_token, session=session)

        if not token_obj:
            return None

        try:
            userid_int = int(token_obj.userid)
        except:
            return None

        if token_obj.aud:
            aud = token_obj.aud.split(",")
            if requesting_client_id not in aud:
                return None

        return await dbfunc.get_user(session, id=userid_int)
    finally:
        if is_new_session:
            session.close()


async def get_scopes_from_bearer_token(bearer_token, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()
        token_obj = await get_bearer_token(bearer_token, session=session)

        if not token_obj:
            return None

        if not token_obj.scopes:
            return None

        return token_obj.scopes
    finally:
        if is_new_session:
            session.close()


async def get_bearer_token(bearer_token, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()
        token_obj = await dbfunc.get_oauth2bearertoken(
            session, access_token=bearer_token
        )

        if not token_obj:
            return None

        if not token_obj.valid:
            return None

        return token_obj
    finally:
        if is_new_session:
            session.close()


async def get_bearer_from_refresh(refresh_token, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()
        return await dbfunc.get_oauth2bearertoken(session, refresh_token=refresh_token)
    finally:
        if is_new_session:
            session.close()


async def create_client(
    email, domain, name, redirect_uri, must_authorize=True, scopes=None, session=None
):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()
        return await dbfunc.create_oauth2client(
            session, email, domain, name, redirect_uri, must_authorize, scopes
        )
    finally:
        if is_new_session:
            session.close()


async def validate_scopes(client_id, scopes, client=None, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()
        if not isinstance(scopes, list):
            if " " in scopes:
                scopes = scopes.split(" ")
            else:
                # Would happen if they didn't use spaces as delimiters
                scopes = scopes.split(",")

        valid_scopes = await get_scopes(client_id, client=client, session=session)

        if not valid_scopes:
            return False, "invalid_client_id_validate_scopes"

        if not scopes:
            return True, ""

        for s in scopes:
            if s == "openid":
                continue

            if s not in valid_scopes:
                return False

        return True, ""
    finally:
        if is_new_session:
            session.close()


async def must_authorize(client_id, client=None, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()
        if not client:
            client = await get_client(client_id, session=session)

        if not client:
            return True

        return client.must_authorize
    finally:
        if is_new_session:
            session.close()


async def get_scopes(client_id, client=None, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()
        if not client:
            client = await get_client(client_id, session)

        if not client:
            return None

        if client.scopes:
            scopes = client.scopes
            return scopes.split(",")
        return ["openid", "uname"]
    finally:
        if is_new_session:
            session.close()


async def get_redirect_url(client_id, client=None, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()
        if not client:
            client = await get_client(client_id)

        return client.redirect_uri
    finally:
        if is_new_session:
            session.close()


async def get_scope_text(scopes, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()
        if isinstance(scopes, str):
            if "," in scopes:
                scopes = scopes.split(",")
            else:
                scopes = scopes.split(" ")

        out = []
        out.append({"name": "Unique ID", "has_access": True})
        if "profile" in scopes:
            out.append({"name": "Display Name", "has_access": True})
            out.append({"name": "Full Name", "has_access": True})
            out.append({"name": "Email", "has_access": True})
            out.append({"name": "Profile Image"})
        else:
            if "email" in scopes:
                out.append({"name": "Email", "has_access": True})
            if "full_name" in scopes:
                out.append({"name": "Display Name", "has_access": True})
                out.append({"name": "Full Name", "has_access": True})
            elif "uname" in scopes:
                out.append({"name": "Display Name", "has_access": True})
            if "image" in scopes:
                out.append({"name": "Profile Image"})

        for scope in scopes:
            if scope.startswith("field."):
                scope = scope[len("field.") :]
                scope_obj = await dbfunc.get_custom_field(
                    session, programmatic_name=scope
                )
                out.append({"name": scope_obj.display_name, "has_access": True})
            else:
                scope_objs = await dbfunc.get_all_custom_field(
                    session, include_with_scopes=scope
                )
                for obj in scope_objs:
                    out.append({"name": obj.display_name, "has_access": True})

        out.append({"name": "Password", "has_access": False})
        return out
    finally:
        if is_new_session:
            session.close()


async def get_client_name(client_id, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()
        client = await get_client(client_id)

        if not client:
            return None

        return client.name
    finally:
        if is_new_session:
            session.close()


async def validate_client_id(
    client_id, redirect_url=None, client_secret=None, session=None
):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()

        client = await get_client(client_id, session=session)

        if not client:
            return False

        if redirect_url:
            if client.redirect_uri:
                if client.redirect_uri != redirect_url:
                    return False

        if client_secret and client.client_secret != client_secret:
            return False

        return True
    finally:
        if is_new_session:
            session.close()


async def get_client_with_secret(client_secret, client_id=None, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()

        obj = await dbfunc.get_oauth2client(session, client_secret=client_secret)

        if not obj:
            return None

        if client_id:
            return obj.client_id == client_id

        return obj
    finally:
        if is_new_session:
            session.close()


async def get_client(client_id, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()

        return await dbfunc.get_oauth2client(session, client_id=client_id)
    finally:
        if is_new_session:
            session.close()


async def gen_jwt_keys():
    public_key, private_key = await cryptrand.generate_ecc_keys()

    await config.set_value("JWT_PUBKEY", public_key.decode("UTF-8"))
    await config.set_value("JWT_SECRET", private_key.decode("UTF-8"))

    return public_key, private_key


async def load_jwt_keys():
    global JWT_PUBKEY
    global JWT_SECRET

    public_key = await config.get_value("JWT_PUBKEY")
    private_key = await config.get_value("JWT_SECRET")

    if not public_key:
        public_key, private_key = await gen_jwt_keys()
    else:
        public_key = public_key.encode("UTF-8")

    public_key_json = JWK()
    public_key_json.import_from_pem(public_key)

    JWT_PUBKEY = ujson.loads(public_key_json.export_public())
    JWT_SECRET = private_key


asyncio.get_event_loop().run_until_complete(load_jwt_keys())
