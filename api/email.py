from db import func as dbfunc, new_session

from security import passwords


async def verify_email(token, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()
        user = await dbfunc.get_user(session, email_verify_token=token)

        if not user:
            return False

        user.verified = True
        user.email_verify_token = None
        user.save(session)

        return True
    finally:
        if is_new_session:
            session.close()


async def password_reset(token, password, passconf, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()
        if password != passconf:
            return "invalid_password"

        user = await dbfunc.get_user(session, email_reset_token=token)

        if not user:
            return "invalid_user"

        hashpass = passwords.hash_password(password)

        user.password = hashpass
        user.save(session)

        return None
    finally:
        if is_new_session:
            session.close()
