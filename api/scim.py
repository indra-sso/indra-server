import asyncio


from db import func as dbfunc, new_session

from serverconf import config

from api import userauth
from security import passwords


async def search_user(query, limit, offset, order_by, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()

        users, count = await dbfunc.search_user(session, query, limit, offset, order_by)

        out = [u.as_dict() for u in users]

        return count, out
    finally:
        if is_new_session:
            session.close()


async def update_user(userid, email, fname, lname, uname, groups, image, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()

        user = await get_user_obj(userid, session=session)

        if not user:
            return

        if email:
            user.email = email

        if fname:
            user.fname = fname

        if lname:
            user.lname = lname

        if uname:
            user.uname = uname

        if groups:
            user.groups = groups

        if image:
            user.image = image

        user.save(session)
    finally:
        if is_new_session:
            session.close()


async def update_user_fields(userid, fields, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()

        user = await get_user_obj(userid, session=session)

        if not user.custom_fields:
            user.custom_fields = {}

        for key in fields:
            user.custom_fields[key] = fields[key]

        user.custom_fields = user.custom_fields  # Yay for sqlalchemy being... weird
        user.flag_modified_custom_fields()
        user.save(session)
    finally:
        if is_new_session:
            session.close()


async def get_user_by_fields(fields, limit=100, offset=0, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()

        if not limit:
            limit = 100
        if limit > 1000:
            limit = 1000
        if not offset:
            offset = 0

        users = await dbfunc.get_user_by_fields(
            session, fields, limit=limit, offset=offset
        )

        if not users:
            return []

        outusers = []
        for user in users:
            d = user.as_dict()
            if not d["image"]:
                d["image"] = await config.get_value("PROFILE_DEFAULT_IMAGE")
            outusers.append(d)

        return outusers
    finally:
        if is_new_session:
            session.close()


async def get_user_by_groups(groups, limit=100, offset=0, order_by=None, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()

        if isinstance(groups, str):
            groups = groups.split(",")

        if not limit:
            limit = 100
        if limit > 1000:
            limit = 1000
        if not offset:
            offset = 0

        users, total = await dbfunc.get_user_by_groups(
            session, groups, limit=limit, offset=offset, order_by=order_by
        )
        if not users:
            return []

        out = []
        for u in users:
            d = u.as_dict()
            if not d["image"]:
                d["image"] = await config.get_value("PROFILE_DEFAULT_IMAGE")
            out.append(d)
        return {"total": total, "rows": out}
    finally:
        if is_new_session:
            session.close()


async def create_user(email, password, fname, lname, uname, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()

        return await userauth.handle_signup(
            email, password, password, fname, lname, uname, session=session
        )
    finally:
        if is_new_session:
            session.close()


async def get_user(userid, limit=100, offset=0, order_by=None, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()

        if userid:
            return await get_user_by_userid(userid, session=session)

        if not limit:
            limit = 100
        if limit > 1000:
            limit = 1000
        if not offset:
            offset = 0

        users = await dbfunc.get_all_users(
            session, limit=limit, offset=offset, order_by=order_by
        )

        out_users = []
        for user in users:
            out_users.append(user_to_dict(user))

        return await asyncio.gather(*out_users)
    finally:
        if is_new_session:
            session.close()


async def get_user_by_userid(userid, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()

        user = await dbfunc.get_user(session, id=userid)
        if not user:
            return None

        return await user_to_dict(user)
    finally:
        if is_new_session:
            session.close()


async def get_user_by_email(email, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()

        user = await dbfunc.get_user_by_email(session, email)
        if not user:
            return None

        return await user_to_dict(user)
    finally:
        if is_new_session:
            session.close()


async def user_to_dict(user_obj):
    d = user_obj.as_dict()

    if not d["image"]:
        d["image"] = await config.get_value("PROFILE_DEFAULT_IMAGE")
    return d


async def get_user_obj(userid, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()

        return await dbfunc.get_user(session, id=userid)
    finally:
        if is_new_session:
            session.close()


async def update_password(userid, password, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()

        user = await dbfunc.get_user(session, id=userid)

        user.password = passwords.hash_password(password)

        user.save(session)
    finally:
        if is_new_session:
            session.close()
