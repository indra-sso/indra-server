import asyncio


from db import func as dbfunc, new_session

from security import passwords, cryptrand

from contact import email as emailapi

from serverconf import config

UNAME_SHOULD_BE_UNIQUE = (
    asyncio.get_event_loop().run_until_complete(
        config.get_value("UNAME_SHOULD_BE_UNIQUE")
    )
    == "TRUE"
)


async def handle_signin(email, password, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()

        d = {
            "signedin": False,
            "userid": None,
            "tfa_needed": False,
            "tfa_types_avail": [],
            "error": "",
        }

        user = await dbfunc.get_user_by_email(session, email)
        if not user:
            return d

        if not user.verify_password(password):
            return d

        if user.groups:
            if await config.get_value("BANNED_GROUP") in user.groups.split(","):
                d["signedin"] = False
                d["error"] = "banned"
                return d

        if not user.tfa_enabled:
            d["signedin"] = True
            d["userid"] = user.id
            return d

        d["tfa_needed"] = True
        d["userid"] = user.id
        d["tfa_types_avail"] = user.tfa_avail.split(",")
        return d
    finally:
        if is_new_session:
            session.close()


async def handle_signout():
    return


async def handle_signup(email, password, passconf, fname, lname, uname, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()

        user = await dbfunc.get_user_by_email(session, email)
        if user:
            return False, "email_taken"
        if UNAME_SHOULD_BE_UNIQUE:
            user = await dbfunc.get_user_by_uname(session, uname)
            if user:
                return False, "uname_taken"

        if password != passconf:
            return False, "password_doesnt_match"

        hashpass = passwords.hash_password(password)

        user = await dbfunc.create_user(session, email, hashpass, fname, lname, uname)
        user.save()

        verify_token = user.email_verify_token

        if await config.get_value("USERS_NEED_VERIFY") == "TRUE":
            await emailapi.send_verify_email(
                user.email, "{} {}".format(user.fname, user.lname), verify_token
            )
        else:
            user.verify_token = ""
            user.verified = True
            user.save(session)

        return True, ""
    finally:
        if is_new_session:
            session.close()


async def invalidate_session(session_id, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()

        await dbfunc.invalidate_oauth2_pkcecode(session, session_id)
        await dbfunc.invalidate_oauth2_bearertoken(session, session_id)
        await dbfunc.invalidate_oauth2_authorizationcode(session, session_id)
    finally:
        if is_new_session:
            session.close()


async def forgot_password(email, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()

        user = await dbfunc.get_user_by_email(session, email)

        if not user:
            return

        reset_token = await cryptrand.gen_crypto_chars(16)

        user.email_reset_token = reset_token

        user.save(session)

        await emailapi.send_reset_email(
            user.email, "{} {}".format(user.fname, user.lname), reset_token
        )
    finally:
        if is_new_session:
            session.close()


async def reset_password(reset_token, newpass, passconf, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()

        user = await dbfunc.get_user(session, email_reset_token=reset_token)

        if newpass != passconf:
            return False, "passwords_do_not_match"

        if not reset_token:
            return False, "invalid_reset_token"

        hashpass = passwords.hash_password(newpass)

        user.password = hashpass
        user.email_reset_token = ""

        user.save(session)

        return True, ""
    finally:
        if is_new_session:
            session.close()
