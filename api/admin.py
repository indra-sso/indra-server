from db import func as dbfunc, new_session


from serverconf import config


async def validate_api_key(api_key, scope, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()
        key_obj = await dbfunc.get_apikey(session, key=api_key)

        if not key_obj:
            return False

        if scope not in key_obj.scope:
            return False

        if not key_obj.valid:
            return False

        return True
    finally:
        if is_new_session:
            session.close()


async def create_custom_field(name, display_name, include_with_scopes, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()

        await dbfunc.create_custom_field(
            session, name, display_name, include_with_scopes
        )
    finally:
        if is_new_session:
            session.close()


async def impersonate_user(src_userid, dst_userid, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()

        src_user = await dbfunc.get_user(session, id=src_userid)
        dst_user = await dbfunc.get_user(session, id=dst_userid)
        d = {"successful": False, "error": ""}

        admin_group = await config.get_value("ADMIN_GROUP")

        if not src_user.groups or admin_group not in src_user.groups.split(","):
            d["error"] = "unauthorized"
            return d

        if not dst_user:
            d["error"] = "user_not_found"
            return d

        d["successful"] = True
        return d
    finally:
        if is_new_session:
            session.close()
