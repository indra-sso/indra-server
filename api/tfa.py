import pyotp

from db import func as dbfunc, new_session

from security import cryptrand

from contact import twilio


async def handle_tfa_signin(userid, tfa_type, tfa_data, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()

        if tfa_type == "otp":
            user = await dbfunc.get_user(session, id=userid)
            totp = pyotp.TOTP(user.tfa_otp_secret)
            return totp.verify(tfa_data), ""

        else:
            return False, "unknown_tfa_type"
    finally:
        if is_new_session:
            session.close()


async def enable_tfa(userid, tfa_type, additional_data=None, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()

        d = {"success": False, "message": "invalid_tfa_type", "tfa_data": {}}
        if tfa_type == "otp":
            secret, qrcode = await enable_tfa_otp(userid, session=session)
            if secret:
                d["success"] = True
                d["message"] = ""
                d["tfa_data"]["secret"] = secret
                d["tfa_data"]["qrcode"] = qrcode
        elif tfa_type == "sms":
            if (
                "phone_number" not in additional_data
                or not additional_data["phone_number"]
            ):
                d["success"] = False
                d["message"] = "invalid_phone_number"
                return

            ret, msg = await enable_tfa_sms(
                userid, additional_data["phone_number"], session=session
            )
            if ret:
                d["success"] = True
            else:
                d["message"] = msg

        return d
    finally:
        if is_new_session:
            session.close()


async def enable_tfa_otp(userid, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()

        user = await dbfunc.get_user(session, id=userid)
        if not user:
            return "", ""

        user.tfa_otp_secret = pyotp.random_base32()
        user.save(session)

        email = user.email

        qrcode = pyotp.totp.TOTP(user.tfa_otp_secret).provisioning_uri(
            email, issuer_name="Indra"
        )

        return user.tfa_otp_secret, qrcode
    finally:
        if is_new_session:
            session.close()


async def enable_tfa_sms(userid, phone_number, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()

        user = await dbfunc.get_user(session, id=userid)
        if not user:
            return None, "invalid_user"

        tfa_code = await cryptrand.generate_numeric_code(6)
        user.tfa_sms_code = tfa_code
        user.tfa_sms_phone_number = phone_number
        user.save(session)

        await twilio.send_tfa_confirmation(phone_number, tfa_code)

        return True, ""
    finally:
        if is_new_session:
            session.close()


async def verify_enable_tfa(userid, tfa_type, data, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()
        if tfa_type == "otp":
            return await verify_enable_tfa_otp(userid, data, session=session)
        elif tfa_type == "sms":
            return await verify_enable_tfa_sms(userid, data, session=session)

    finally:
        if is_new_session:
            session.close()


async def verify_enable_tfa_otp(userid, code, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()

        user = await dbfunc.get_user(session, id=userid)
        if not user:
            return False, "invalid_user"

        if not pyotp.totp.TOTP(user.tfa_otp_secret).verify(code):
            return False, "invalid_code"

        user.tfa_enabled = True
        if not user.tfa_avail:
            user.tfa_avail = "otp"
        elif "otp" not in user.tfa_avail:
            tfa_avail = user.tfa_avail.split(",")
            tfa_avail.append("otp")
            user.tfa_avail = ",".join(tfa_avail)
        user.save(session)

        return True, ""
    finally:
        if is_new_session:
            session.close()


async def verify_enable_tfa_sms(userid, code, session=None):
    is_new_session = False
    try:
        if not session:
            is_new_session = True
            session = new_session()

        user = await dbfunc.get_user(session, id=userid)
        if not user:
            return False, "invalid_user"

        if code != user.tfa_sms_code:
            return False, "invalid_code"

        user.tfa_enabled = True
        if not user.tfa_avail:
            user.tfa_avail = "sms"
        elif "sms" not in user.tfa_avail:
            tfa_avail = user.tfa_avail.split(",")
            tfa_avail.append("sms")
            user.tfa_avail = ",".join(tfa_avail)

        user.tfa_sms_code = ""
        user.save(session)

        return True, ""
    finally:
        if is_new_session:
            session.close()
