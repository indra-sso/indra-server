from pydantic import BaseModel, EmailStr, HttpUrl, field_validator


class OauthClientModel(BaseModel):
    email: EmailStr
    domain: HttpUrl
    name: str
    redirect_uri: HttpUrl

    @field_validator("name")
    @classmethod
    def name_length(cls, value):
        if len(value) <= 4:
            raise ValueError("Name must be longer than 4 characters")
        return value
