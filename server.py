import asyncio
import logging
import os
import sys
import argparse

import aiohttp_jinja2
import aiohttp_session
import aiohttp_session.redis_storage
import jinja2
from redis import asyncio as aioredis
import ujson

from aiohttp import web

import db

import serverconf


from endpoints import add_routes
from endpoints.middleware import authm


stdio_handler = logging.StreamHandler()
stdio_handler.setLevel(logging.INFO)
_logger = logging.getLogger("aiohttp.access")
_logger.addHandler(stdio_handler)
_logger.setLevel(logging.DEBUG)

# logging.basicConfig()   # log messages to stdout
# logging.getLogger('sqlalchemy.dialects.postgresql').setLevel(logging.INFO)


async def config_processor(request):
    d = {}
    d["server_name"] = await serverconf.config.get_value("SERVER_NAME")
    d["server_title_name"] = await serverconf.config.get_value("SERVER_TITLE_NAME")
    d["static_url"] = await serverconf.config.get_value("STATIC_URL")
    return d


async def init_tasks():
    # await db.func.create_user("eamonn.nugent@demilletech.net", "password", "Eamonn", "Nugent", "enugent")
    # await api.scim.create_user("syam@poscon.net", "password", "Syam", "Test", "Syam") #this will create a new SSO user
    print(
        await db.func.create_apikey(scope="scim_admin")
    )  # this will create a new API key for use to query SSO
    # await api.userauth.handle_signup("eamonn.nugent@demilletech.net", "password", "password", "Eamonn", "Nugent", "enugent")
    # await api.oauth.create_client("wm@poscon.net", "operators.poscon.com", "POSCON Operators")
    # await db.func.get_user_by_groups(["0"])
    # await db.func.create_apikey(scope="scim_readonly")
    # print(await api.scim.search_user("eamonn"))

    # await db.audit.create_audit_log_genesis()
    # obj = await db.audit.create_audit_log_entry("testy", "testmctestface", "test123")
    # print(obj)
    # assert await db.audit.verify_signature_chain()

    # from contact import twilio

    # await twilio.send_text("+19876543210", "Hi, AJ. It's working!")

    return


async def application_factory():
    print("Connecting to Redis")
    try:
        redis = await aioredis.from_url(os.environ.get("REDIS_HOST"))
    except FileNotFoundError:
        print("Redis server could not be found")
        exit()

    print("Connected to Redis")

    await serverconf.init()

    app = web.Application()

    template_dir = await serverconf.config.get_value("TEMPLATES_DIR")
    template_rel_dir = "templates/{}".format(template_dir)
    template_dist_dir = "{}/dist".format(template_rel_dir)

    aiohttp_jinja2.setup(
        app,
        context_processors=[config_processor, aiohttp_jinja2.request_processor],
        loader=jinja2.FileSystemLoader(template_rel_dir),
    )

    storage = aiohttp_session.redis_storage.RedisStorage(
        redis, cookie_name="s", max_age=604800, encoder=ujson.dumps, decoder=ujson.loads
    )  # 1 week in seconds

    aiohttp_session.setup(app, storage)
    app.middlewares.append(authm.auth_middleware)

    await add_routes(app)
    app.add_routes([web.static("/static", "static")])
    if os.path.isdir(template_dist_dir):
        app.add_routes([web.static("/dist", template_dist_dir)])

    return app


def run_plain():
    parser = argparse.ArgumentParser(description="Indra Server")
    parser.add_argument(
        "--profile",
        action="store_const",
        const=True,
        help="Profile the server",
        required=False,
        dest="profile",
    )
    parser.add_argument(
        "--init-tasks",
        action="store_const",
        const=True,
        help="Run initialization tasks",
        required=False,
        dest="init_tasks",
    )
    args = parser.parse_args()

    app = asyncio.get_event_loop().run_until_complete(application_factory())

    if args.init_tasks:
        asyncio.get_event_loop().run_until_complete(init_tasks())

    if args.profile:
        import cProfile

        profiler = cProfile.Profile()
        profiler.enable()
        try:
            print("Running app")
            sys.exit(web.run_app(app))
        finally:
            profiler.dump_stats("server.prof")
            profiler.disable()
    else:
        print("Running app")
        web.run_app(app)


if __name__ == "__main__":
    run_plain()
