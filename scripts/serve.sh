#!/bin/env bash

set -o pipefail

./scripts/env.sh

nginx -c /app/conf/nginx.conf

pipenv run gunicorn server:application_factory --bind unix:/opt/gunicorn/gunicorn.sock --worker-class aiohttp.GunicornWebWorker