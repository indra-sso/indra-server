from db import func as dbfunc, new_session

confs = {}


async def get_value(name):
    session = new_session()
    try:
        global confs
        if name in confs:
            return confs[name]

        value_obj = await dbfunc.get_confoption(session, name=name)
        if not value_obj:
            return None

        confs[name] = value_obj.value

        return value_obj.value
    finally:
        session.close()


async def set_value(name, value):
    session = new_session()
    try:
        await dbfunc.create_confoption(session, name, value)
        confs[name] = value
    finally:
        session.close()
