from contact import email
from serverconf import config

DEFAULTS = {
    "EMAIL_API_TYPE": "SMTP",
    "EMAIL_FROM_ADDRESS": "no-reply@example.com",
    "EMAIL_FROM_NAME": "Example No-Reply",
    "EMAIL_FROM_TEAM_NAME": "The SSO Team",
    "EMAIL_BUSINESS_ADDRESS": "Example SSO\n221b Baker St\n Marylebone, London\n NW1 6XE, UK",
    "EMAIL_HOME_URL": "https://example.com",
    "EMAIL_LOGO_URL": "https://example.com/logo.png",
    "SITE_DOMAIN": "sso.example.com",
    "TEMPLATES_DIR": "default",
    "SERVER_NAME": "Example",
    "SERVER_TITLE_NAME": "Example SSO",
    "UNAME_SHOULD_BE_UNIQUE": "TRUE",
    "USERS_NEED_VERIFY": "TRUE",
    "STATIC_URL": "",
    "FAVICON_URL": "/static/img/favicon.ico",
    "PROFILE_IMAGE_URL_BASE": "https://cdn.example.com/userimages",
    "PROFILE_DEFAULT_IMAGE": "default/member.png",
    "ADMIN_GROUP": "admin",
    "BANNED_GROUP": "banned",
}


async def init():
    if await config.get_value("IS_SETUP") != "true":
        await set_defaults()
    else:
        await ensure_defaults()

    await email_init()
    return


async def email_init():
    mail_type = await config.get_value("EMAIL_API_TYPE")
    from_address = await config.get_value("EMAIL_FROM_ADDRESS")
    from_name = await config.get_value("EMAIL_FROM_NAME")
    await email.init(mail_type, from_address, from_name)


async def ensure_defaults():
    for key in DEFAULTS:
        if await config.get_value(key) is None:
            await config.set_value(key, DEFAULTS[key])


async def set_defaults():
    for key in DEFAULTS:
        await config.set_value(key, DEFAULTS[key])

    await config.set_value("IS_SETUP", "true")
